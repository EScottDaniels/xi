/*
=================================================================================================
	(c) Copyright 1995-2018 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* 
* Mnemonic: XIdrawtext
* Abstract: Text drawing functions.
* Date:     30 July 1991 (PORT FOM VMS)
* Author:   E. Scott Daniels
*
*	Mods:	08 Jul 2018 - Added back buffering support.
*
****************************************************************************
*/
#include "xiinclud.h"

/*
	Draws the given string without affecting any other pixels in the drawing area
	other than those used to draw the characters themselves.
*/
extern int XIdrawtext( int window, int x, int y, char *str )
{
	struct window_blk *wptr;  /* pointer at window block of information */

	if( (wptr= XIvalidw( window )) == NULL )
		return XI_ERR_BADWNUM;   

	x += wptr->orig_x;
	y += wptr->orig_y;

	XDrawString( dlist[wptr->dnum]->disp, wptr->window, wptr->gc, x, y, str, strlen( str ) );
	//XFlush( dlist[wptr->dnum]->disp );       

	return XI_OK;
}                   

/*
	Draws the given string after filling the bounding box with the current background
	colour.
*/
extern int XIdrawstring( int window, int x, int y, char *str )
{
	struct window_blk *wptr;  /* pointer at window block of information */

	if( (wptr= XIvalidw( window )) == NULL )
		return XI_ERR_BADWNUM;   

	x += wptr->orig_x;
	y += wptr->orig_y;

	XDrawImageString( dlist[wptr->dnum]->disp, wptr->window, wptr->gc, x, y, str, strlen( str ) );
	//XFlush( dlist[wptr->dnum]->disp );       

	return XI_OK;
}                   


/*
	Draws the given string without affecting any other pixels in the drawing area
	other than those used to draw the characters themselves. The string is written to the
	back buffer so it will not be visible until the buffers are swapped.
*/
extern int XIbb_drawtext( int window, int x, int y, char *str )
{
	struct window_blk *wptr;  /* pointer at window block of information */

	if( (wptr= XIvalidw( window )) == NULL )
		return XI_ERR_BADWNUM;   

	XDrawString( dlist[wptr->dnum]->disp, wptr->bbuffer, wptr->gc, x, y, str, strlen( str ) );

	return XI_OK;
}                   


#ifdef KEEP
/* draw text to an image's pixmap rather than to a window 
	still necessary to pass in a window as that is our link to display
*/
extern int XIimdrawtext( int window, XImage *image, int x, int y, char *str )
{
	struct window_blk *wptr;  /* pointer at window block of information */

	if( image == NULL || (wptr= XIvalidw( window )) == NULL )
		return XI_ERR_BADWNUM;   

fprintf( stderr, "*** WARN *** XIimdrawtext is not supported at this time; call ignored\n" );
	//XDrawImageString( dlist[wptr->dnum]->disp, image, wptr->gc, x, y, str, strlen( str ) );
	//XFlush( dlist[wptr->dnum]->disp );       

	return XI_OK;
}                   
#endif



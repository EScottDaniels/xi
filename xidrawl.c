/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
*
* Mnemonic: XIdrawl
* Abstract: This routine is responsible for drawing a line from x1 y1 to 
*           x2 y2 in the indicated window.
* Parms:    window - Window number ( index into xiwins)
*           x1     - coordinates of first point
*           y1
*           x2     - Coordinates of second point
*           y2
*           option - Flush option 
* Date:     25 July 1991 (PORT FOM VMS)
* Author:   E. Scott Daniels
*
*	Mods:	03 Jul 2018 - Add double buffering support
*
****************************************************************************
*/
#include "xiinclud.h"

extern int XIdrawl( int window, int x1, int y1, int x2, int y2, int option ) {
	struct window_blk *wptr;    /* pointer at the window information */
	Drawable target;

	if( (wptr = XIvalidw( window )) == NULL )
		return( XI_ERR_BADWNUM );
 
	if( option & XI_OPT_BB ) {				// select target based on options
		target = wptr->bbuffer;
	} else {
		target = wptr->window;
	}

	x1 += wptr->orig_x;
	y1 += wptr->orig_y;
	x2 += wptr->orig_x;
	y2 += wptr->orig_y;

	XDrawLine( dlist[wptr->dnum]->disp, target, wptr->gc, x1, y1, x2, y2 );

	if( option & XI_OPT_FLUSH )
		XFlush( dlist[wptr->dnum]->disp );  /* flush if requested */

	return XI_OK;
}                /* XIdrawl */

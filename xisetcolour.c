/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic:  XIsetcolour
* Abstract: Set and get colour funcitons
* Date:     25 July 1991 (PORT FOM VMS)
* Author:   E. Scott Daniels
* Mods:		19 Jun 2018 Added fetch_rgb function.
*
****************************************************************************
*/
#include "xiinclud.h"

/*
	Sets the colour in the given window.  Subsequent drawing actions will
	use this colour until changed.
	Returns XI_OK if colour was set; error otherwise.
*/
int XIsetcolour( int wnum, int colour ) {
	struct window_blk *wptr;   /* poiner at the window management block */
	colour_t*	cinfo;

	if( (wptr = XIvalidw( wnum )) == NULL ) {  /* good xi window? */
		return( XI_ERR_BADWNUM );               /* tell the caller */
	}

	if( colour >= 0 && colour < MAX_COLOURS * 3 ) {
		if( (cinfo = dlist[wptr->dnum]->clut[colour]) == NULL ) {
			return XI_ERR_BADVAL;
		}

		wptr->active_colour = colour;
		XSetForeground( dlist[wptr->dnum]->disp, wptr->gc, cinfo->hwcolour.pixel );	// set the x pixle value
	} else {
		return XI_ERR_BADVAL;
	}

	return XI_OK;
}

/*
	For the given window, find the colour id in the our clut and return the 
	r,g,b value using the pointers given.  If colour id is out of range,
	then the active colour information is returned.

	Returns XI_OK if r/g/b were set; error otherwise.
*/
int XIget_colour_rgb( int wnum, int colour_id, unsigned short* r, unsigned short* g, unsigned short* b ) {
	struct window_blk *wptr;						// what we map wnum to to reference display
	colour_t*	cinfo;

	if( (wptr = XIvalidw( wnum )) == NULL ) {
		return( XI_ERR_BADWNUM );
	}

	if( colour_id < 0 || colour_id > MAX_COLOURS * 3 ) {			// default if not in range
		colour_id = wptr->active_colour;
	}

	if( (cinfo = dlist[wptr->dnum]->clut[colour_id]) == NULL ) {
		return XI_ERR_BADVAL;
	}

	if( r ) {
		*r = cinfo->hwcolour.red;
	}
	if( g ) {
		*g = cinfo->hwcolour.green;
	}
	if( b ) {
		*b = cinfo->hwcolour.blue;
	}

	return XI_OK;
}


/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XIdrawb
* Abstract: This routine is responsible for drawing a 3 dimensional
*           "button" in the given window.
* Parms:    wnum - window number (index into xiwins.)
*           ulx  - X cordinate in the window 
*           uly  - Y coordinate in the window
*           height- Height of the button
*           Width - Width of the button
*           colour - Button Color
*           text  - Pointer to text string to write on button
*           option- flush and background options.     
* Date:     11 September 1991 
* Author:   E. Scott Daniels
*
* Mods:		12 Dec 2018 - Support drawing buttons into backing buffer for double buffering
*
****************************************************************************
*/
#include "xiinclud.h"

extern int XIdrawb( int wnum, int ulx, int uly, int height, int width, int colour, int tcolour, char *text, int option ) {
	struct window_blk *wptr;    		/* window information */
	int	bb_option = XI_NOOPTION;

	if( (wptr = XIvalidw( wnum )) == NULL ) {
		return XI_ERR_BADWNUM;
	}

	// draw rect/text will shift we do NOT need to adjust x,y
	
	height *= wptr->scale;
	width *= wptr->scale;
	ulx *= wptr->scale;
	uly *= wptr->scale;

	if( option & XI_OPT_BB ) {				// select target based on options
		bb_option = XI_OPT_BB;
	}

	if( option & XI_OPT_BACKGROUND ) {   /* need to provide a background? */
		XIsetcolour( wnum, colour );								        /* set users colour for background */
		XIdrawr( wnum, ulx-10, uly-10, height+20, width+20, XI_OPT_FILL );
	}

	if( option & XI_OPT_IN ) {     /* draw button pressed in? */
		XIsetcolour( wnum, XI_BLACK ); 					/* shadow edging along the top and right side */
		XIdrawl( wnum, ulx, uly, ulx+width-2, uly, bb_option );
		XIdrawl( wnum, ulx, uly, ulx, uly+height-2, bb_option );

		XIsetcolour( wnum, colour + XI_LIGHT );					/* highlight edging along the left and bottom */
		XIdrawr( wnum, ulx+1, uly+1, height-1, width-1, XI_OPT_FILL | bb_option );
		XIsetcolour( wnum, colour + XI_DARK );   				/* set to users colour for button */
		XIdrawr( wnum, ulx+1, uly+1, height-2, width-2, XI_OPT_FILL | bb_option );
	} else {
		XIsetcolour( wnum, colour+XI_LIGHT );   
		XIdrawl( wnum, ulx, uly, ulx+width-2, uly, bb_option );
		XIdrawl( wnum, ulx, uly, ulx, uly+height-2, bb_option );

		XIsetcolour( wnum, XI_BLACK );
		XIdrawr( wnum, ulx+1, uly+1, height-1, width-1, XI_OPT_FILL | bb_option );
		XIsetcolour( wnum, colour );   /* set to users color for button */
		XIdrawr( wnum, ulx+1, uly+1, height-2, width-2, XI_OPT_FILL | bb_option );
	}

	if( text && strcmp( text, "" ) ) {				// text supplied and not empty string
		XIsetcolour( wnum, tcolour );
		if( option & XI_OPT_IN ) {     					/* shift text slightly on an indented button */
			//XIdrawtext( wnum, ulx+7, uly+((2*height)/3)+1, text );
			XIxft_draw_text( wnum, ulx+7, uly+((2*height)/3)+1, XI_ACTIVE_COLOUR, XI_ACTIVE_FONT,  text, bb_option );
		} else {
			XIxft_draw_text( wnum, ulx+5, uly+((2*height)/3), XI_ACTIVE_COLOUR, XI_ACTIVE_FONT,  text, bb_option );
			//XIdrawtext( wnum, ulx+5, uly+((2*height)/3), text );
		}
	}

	if ( option & XI_OPT_FLUSH ) {           /* if flush option on */
		XFlush( dlist[wptr->dnum]->disp );   /* flush buffer to window manager */
	}

	return XI_OK;
}

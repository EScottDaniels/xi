/*
=================================================================================================
	(c) Copyright 1995-2018 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
***************************************************************************
*
*  Mnemonic:	XIfonts (xisetfon.c)
*  Abstract:	Font management functions. This supports both the old X11 core
*				font management. The better approach, using xft, is supported in 
				the xixft.c module. 
*  Date:		28 January 1997
*  Author:		E. Scott Daniels
*
***************************************************************************
*/
#include "xiinclud.h"


static font_t fonts[MAX_FONTS];
static int font_ip = 0;						// insertion point

/*
	Load the named font.  (Original, unflexible)
*/
int XIsetfont( int wnum, char *fname )
{
 struct window_blk *wptr;          /* pointer at the window block */
 int status = XI_ERROR;            /* return status */

 if( (wptr = XIvalidw( wnum )) != NULL )   /* if valid window number */
  {
   wptr->font = XLoadQueryFont( dlist[wptr->dnum]->disp, fname );
   if( wptr->font != XERROR )                          /* X load ok? */
    {
     XSetFont( dlist[wptr->dnum]->disp, wptr->gc, wptr->font->fid );
     status = XI_OK;
    }
  }

 return( status );
}                      /* XIsetfont */

/*
	XIfind_font will search the current font list for the named font and return the font number
	that can be used on an XIuse_font() call. If the font is not known, it will be loaded. 
	This needs to do a search over the whole table; the user application really should save
	and map font numbers in a hash table, or manage the font number with the text.

	If there is an error, XI_ERROR is returned.
*/
extern int XIfind_font( int wnum, char* font_name ) {
	int i;
	XFontStruct *font;
	struct window_blk *wptr;			// window information

	if( font_name == NULL ) {
		return XI_ERROR;
	}

	if( font_ip >= MAX_FONTS ) {
		return XI_ERROR;
	}

	for( i = 0; i < font_ip; i++ ) {
		if( strcmp( font_name, fonts[i].name ) == 0 ) {
			return i;
		}
	}

	if( (wptr = XIvalidw( wnum )) != NULL ) {
		font = XLoadQueryFont( dlist[wptr->dnum]->disp, font_name );				// attempt load; save stuff if good
		if( font != XERROR ) {
			fonts[font_ip].name = malloc_dup( font_name );		// strdup isn't std, this is our flavour
			fonts[font_ip].font = font;
			font_ip++;
			return font_ip - 1;
		}
	}

	return XI_ERROR;
}


/*
	XIuse_font will activate the font with the given font number. If the number
	isn't valid, XI_ERROR is returned.
*/
extern int XIuse_font( int wnum, int font_id ) {
	struct window_blk *wptr;						// window data

	if( font_id < 0 || font_id >= font_ip ) {
		return XI_ERROR;
	}

	if( (wptr = XIvalidw( wnum )) != NULL ) {
		wptr->font = fonts[font_id].font;
		XSetFont( dlist[wptr->dnum]->disp, wptr->gc, wptr->font->fid );
		return XI_OK;
	}

	return XI_ERROR;
}
	
/*
	XIdump_font_info dumps some font info to standard error and then returns.
	Mostly for debugging or help.
*/
extern void XIdump_font_info( int wnum, char* pattern ) {
	struct window_blk *wptr;
	int		nlist;		// number of things put into the list
	int		i;
	char** list;

	if( (wptr = XIvalidw( wnum )) != NULL ) {
		list = XListFonts( dlist[wptr->dnum]->disp, pattern, 128, &nlist );
		if( list ) {
			for( i = 0; i < nlist; i++ ) {
			fprintf( stderr, "font: %s\n", list[i] );
			}

			XFreeFontNames( list );
		} else {
			fprintf( stderr, "no fonts found\n" );
		}
	}
}

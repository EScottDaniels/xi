/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XIdefcolour 
* Abstract: Colour definition functions.
* Parms:    wnum - pointer at display block
* Returns:  XI_OK
* Date:     25 July 1991 (PORT FOM VMS)
* Author:   E. Scott Daniels
*
* Mods:		19 June 2018 - extended to support Xft string drawing.
*
****************************************************************************
*/
#include "xiinclud.h"

Colormap syscolours = 0;                   /* the system colour map information */ 

/*
	Make a colour block given r,g,b (0 - 255) colour values.
	We compute the hardware colour and then allocate it.
	The struct is returned so that we can use it to retrieve an 
	r,g,b tuple when drawing Xft strings.
*/
static colour_t* mk_colour( struct display_blk* dptr, int r, int g, int b, int mode ) {
	colour_t* newc;

	newc = (colour_t *) malloc( sizeof( *newc ) );
	if( newc == NULL ) {
		return NULL;
	}

	memset( newc, 0, sizeof( *newc ) );
	newc->hwcolour.red = (r & 0xff) << 8;		// x is a two byte colour range; shifting up to high byte gives reasonable approximation
	newc->hwcolour.green = (g & 0xff) << 8;		// set the base colour
	newc->hwcolour.blue = (b & 0xff) << 8;

	switch( mode ) {								// lighten/darken if requested
		case XI_LIGHTEN:
			//newc->hwcolour.red = (newc->hwcolour.red + 0x3200) & 0xffff;			// 20% lighter
			//newc->hwcolour.green = (newc->hwcolour.red + 0x3200) & 0xffff;
			//newc->hwcolour.blue = (newc->hwcolour.red + 0x3200) & 0xffff;

			newc->hwcolour.red = (newc->hwcolour.red + 0x4200) & 0xffff;			// 20% lighter
			newc->hwcolour.green = (newc->hwcolour.red + 0x4200) & 0xffff;
			newc->hwcolour.blue = (newc->hwcolour.red + 0x4200) & 0xffff;
			break;

		case XI_DARKEN:
			if( newc->hwcolour.red > 0x3200 ) {
				newc->hwcolour.red -= 0x3200;					// 20% darker
			} else {
				newc->hwcolour.red -= 0;
			}
			if( newc->hwcolour.green > 0x3200 ) {
				newc->hwcolour.green -= 0x3200;
			} else {
				newc->hwcolour.green -= 0;
			}
			if( newc->hwcolour.blue > 0x3200 ) {
				newc->hwcolour.blue -= 0x3200;
			} else {
				newc->hwcolour.blue -= 0;
			}
			break;
	}

	newc->hwcolour.flags = 0x7f;
	XAllocColor( dptr->disp, syscolours, &newc->hwcolour ); 

	return newc;
}

/*
	Defina a colour with r/g/b (0 - 255) values.  The index given is used
	unless the index is < 0 in which case the next slot is allocated. If
	all slots are full, error is returned.  The index used is returned.
*/
int XIdefcolour( int wnum, int clut_idx, int r, int g, int b ) {
	struct display_blk *dptr;
	//XColor hwcolour;         /* maps rgb to hw colour */
	//int status;             /* processing status information */
	struct window_blk *wptr;    /* pointer at window information block */
	//int adjr;			// adjusted colours
	//int adjg;			// adjusted colours
	//int adjb;			// adjusted colours

	if( (wptr = XIvalidw( wnum )) == NULL )
		return( XI_ERR_BADWNUM );    /* let them know they blew it */

	dptr = dlist[wptr->dnum];   		/* at display info */

	if( clut_idx < 0 && dptr->cidx < MAX_COLOURS-1 ) {
		clut_idx = dptr->cidx++;							// allow for easy assignment if desired
	}

	if( clut_idx < 0 || clut_idx >= MAX_COLOURS  )
		return XI_ERROR;


	if( dptr->clut[clut_idx] != NULL ) {
		free( dptr->clut[clut_idx] );
		free( dptr->clut[clut_idx+XI_DARK] );
		free( dptr->clut[clut_idx+XI_LIGHT] );
	}
	dptr->clut[clut_idx] = mk_colour( dptr, r, g, b, XI_NORMAL );
	dptr->clut[clut_idx+XI_LIGHT] = mk_colour( dptr, r, g, b, XI_LIGHTEN );
	dptr->clut[clut_idx+XI_DARK] = mk_colour( dptr, r, g, b, XI_DARKEN );

#ifdef KEEP
	if( dptr->depth > 1 )               /* if a colour system */
	{
		hwcolour.red = r << 8;		// x is a two byte colour range; shifting up to high byte gives reasonable approximation
		hwcolour.green = g << 8;
		hwcolour.blue = b << 8;

		status = XAllocColor( dptr->disp, syscolours, &hwcolour ); 

		if( status != XERROR ) {  			 /* if real colour allocated successfully */
			dptr->clut[clut_idx] = hwcolour.pixel;      /* save it in clut */
		}

		if( (adjr = r + 50) > 255 ) 		// compute lighter colours
			adjr = 255;
		if( (adjg = g + 50) > 255 )
			adjg = 255;
		if( (adjb = b + 50) > 255 )
			adjb = 255;

		hwcolour.red = adjr << 8;			// set lighter colours
		hwcolour.green = adjg << 8;
		hwcolour.blue = adjb << 8;

		status = XAllocColor( dptr->disp, syscolours, &hwcolour ); 

		if( status != XERROR ) {  			 /* if real colour allocated successfully */
			dptr->clut[clut_idx+XI_LIGHT] = hwcolour.pixel;      /* save it in clut */
		}

		if( (adjr = r - 50) < 0 ) 			// compute darker
			adjr = 0;
		if( (adjg = g - 50) < 0 ) 
			adjg = 0;
		if( (adjb = b - 50) < 0 ) 
			adjb = 0;

		hwcolour.red = adjr << 8;			// set darker
		hwcolour.green = adjg << 8;
		hwcolour.blue = adjb << 8;

		status = XAllocColor( dptr->disp, syscolours, &hwcolour ); 

		if( status != XERROR ) {  			 /* if real colour allocated successfully */
			dptr->clut[clut_idx+XI_DARK] = hwcolour.pixel;      /* save it in clut */
		}
	}
#endif

	return clut_idx;
}                   

/*
	Convnience function
*/
static void mk_three_colours( struct display_blk *dptr, int r, int g, int b ) {
	dptr->clut[dptr->cidx] = mk_colour( dptr, r, g, b, XI_NORMAL );
	dptr->clut[dptr->cidx+XI_LIGHT] = mk_colour( dptr, r, g, b, XI_LIGHTEN );
	dptr->clut[dptr->cidx+XI_DARK] = mk_colour( dptr, r, g, b, XI_DARKEN );
	dptr->cidx++;
}

/*
	Initialise the colour look up table for the display. User programmes can add colours
	on the fly overlaying any index used here, or by extending the table up to 1024
	colours of each normal, light and dark.
*/
int XIloadcolour( struct display_blk *dptr ) {

 	syscolours = DefaultColormap( dptr->disp, dptr->screen ); 			/* system supported colours */

	// -- CAUTION: each line below must have a trailing comment where the first token after the comment will be the XI_* constant for the colour.
	//		A script reads this file and generates xi_colours.h from what it finds here.
	mk_three_colours( dptr, 0x00, 0x00, 0x00 );	//    BLACK		// not alpha so that 0/1 are always black and white
	mk_three_colours( dptr, 0xff, 0xff, 0xff );	//    WHITE
	mk_three_colours( dptr, 0x00, 0xFF, 0xFF );	//    AQUA         
	mk_three_colours( dptr, 0x00, 0x00, 0xFF );	//    BLUE         
	mk_three_colours( dptr, 0xa6, 0x00, 0xff );	//    BLUEVIOLET   
	mk_three_colours( dptr, 0x05, 0x04, 0x90 );	//    BLUEPRINT
	mk_three_colours( dptr, 0x99, 0x66, 0x00 );	//    BROWN        
	mk_three_colours( dptr, 0xc6, 0xb4, 0x82 );	//    BROWNSAND    
	mk_three_colours( dptr, 0xdc, 0xa6, 0x13 );	//    BURNTORANGE  
	mk_three_colours( dptr, 0x53, 0x40, 0x1c );	//    CHOCOLATE    
	mk_three_colours( dptr, 0x97, 0x65, 0x56 );	//    COCOA        
	mk_three_colours( dptr, 0x30, 0x30, 0x30 );	//    DARKGREY     
	mk_three_colours( dptr, 0xab, 0x35, 0x9c );	//    DEEPPURPLE   
	mk_three_colours( dptr, 0xFF, 0x00, 0xFF );	//    FUSCHIA      
	mk_three_colours( dptr, 0x80, 0x80, 0x80 );	//    GRAY         
	mk_three_colours( dptr, 0x80, 0x80, 0x80 );	//    GREY         
	mk_three_colours( dptr, 0xd2, 0xcd, 0xff );	//    GREYBLUE     
	mk_three_colours( dptr, 0xde, 0xd1, 0xb9 );	//    GREYBROWN    
	mk_three_colours( dptr, 0x00, 0x80, 0x00 );	//    GREEN        
	mk_three_colours( dptr, 0x33, 0x00, 0x99 );	//    INDAGO       
	mk_three_colours( dptr, 0xCD, 0x92, 0x1C );	//    LATTE        
	mk_three_colours( dptr, 0xae, 0xa6, 0xf6 );	//    LAVENDER     
	mk_three_colours( dptr, 0xe0, 0xe0, 0xc8 ); // 		LCDBG similar to calculator background
	mk_three_colours( dptr, 0x00, 0xC8, 0x00 );	//    LTGREEN      
	mk_three_colours( dptr, 0x00, 0x00, 0xC8 );	//    LTBLUE       
	mk_three_colours( dptr, 0xC8, 0x00, 0x00 );	//    LTRED        
	mk_three_colours( dptr, 0x00, 0xFF, 0x00 );	//    LIME         
	mk_three_colours( dptr, 0xCC, 0x33, 0x99 );	//    MAGENTA      
	mk_three_colours( dptr, 0x80, 0x00, 0x00 );	//    MAROON       
	mk_three_colours( dptr, 0x6b, 0x46, 0xa9 );	//    MIDNIGHTBLUE 
	mk_three_colours( dptr, 0xEF, 0xDD, 0x3D );	//    MUSTARD      
	mk_three_colours( dptr, 0x00, 0x00, 0x80 );	//    NAVY         
	mk_three_colours( dptr, 0x80, 0x80, 0x00 );	//    OLIVE        
	mk_three_colours( dptr, 0xFF, 0x99, 0x00 );	//    ORANGE       
	mk_three_colours( dptr, 0xFF, 0x99, 0xF1 );	//    PIGPINK      
	mk_three_colours( dptr, 0xff, 0x80, 0xff );	//    PINK         
	mk_three_colours( dptr, 0x80, 0x00, 0x80 );	//    PURPLE       
	mk_three_colours( dptr, 0xFF, 0x00, 0x00 );	//    RED          
	mk_three_colours( dptr, 0xFF, 0xF3, 0xD2 );	//    SAND         
	mk_three_colours( dptr, 0x02, 0xa0, 0x90 );	//    SEAGREEN     
	mk_three_colours( dptr, 0xC0, 0xC0, 0xC0 );	//    SILVER       
	mk_three_colours( dptr, 0x72, 0x6d, 0x7b );	//    STEELBLUE    
	mk_three_colours( dptr, 0xFF, 0xDA, 0x91 );	//    TAN          
	mk_three_colours( dptr, 0x00, 0x80, 0x80 );	//    TEAL         
	mk_three_colours( dptr, 0xB0, 0x00, 0xFF );	//    VIOLET       
	mk_three_colours( dptr, 0xFF, 0xFF, 0x00 );	//    YELLOW       
	mk_three_colours( dptr, 0xFE, 0xE2, 0x33 );	//    YELLOWBUS    
	mk_three_colours( dptr, 0xD2, 0xFF, 0x91 );	//    YELLOWGREEN  

	return XI_OK;
}

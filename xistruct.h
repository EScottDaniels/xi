/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XIstruct.h
* Abstract: This file contains the structure definitions for the XI Xwindows
*            runtime toolkit.
* Date:     25 July 1991
*
****************************************************************************
*/

#ifndef __xistruct
#define __xistruct 
#include <X11/extensions/Xdbe.h>
#include <X11/Xft/Xft.h>

typedef struct {				// colour -- we need both a hardware pixel and rgb reference to support native drawing and xft strings
	unsigned short red;
	unsigned short green;
	unsigned short blue;
	XColor	hwcolour;         // hardware colour
} colour_t;

 struct edit_blk            /* block that contols edit in a field window */
  {
   int window;              /* XI window handle of the window */
   int blen;                /* length of text buffer */
   char *buf;               /* buffer to fill */
   int idx;                 /* current index for next char in buffer */
   int didx;                /* first character from buffer displayed */
   int wwidth;              /* width (pixles) of window */
   int cwidth;              /* pixel width of characters (scroll calc) */
   int cheight;             /* character height */
   int bcolor;              /* background color of the block */
   int fcolor;              /* foreground color of the block */
   int ((*cbrtn)());        /* address of routine to callback on escape/ret */
  };


 struct prompt_blk          /* block used to manage a prompt for info */
  {
   struct edit_blk *eptr;   /* pointer to the edit block if there is one */
   int window;              /* window that we opened for prompt */
   char *text;              /* text for repaint */
   int fcolor;              /* foreground color */
   int bcolor;              /* background color */
   int ((*cbrtn)());        /* address of completion callback routine */
   void *cbdata;            /* data pointer to pass completion callback */
  };

 struct callback_blk        /* information needed to make a callback */
   {
    int ((*cbrtn)());       /* pointer to the callback routine */
    void ((*ecbrtn)( struct edit_blk*, int, int, XEvent* ));      /* pointer to the event callback routine */
    void *cbdata;           /* data to pass to the callback routine */
   };

 struct display_blk
   {                        /* contains display related information */
    Display *disp;          /* pointer to the display struct */
    int screen;             /* screen id returned by X */
	Visual* visual;			// the visual in use for the display
    int width;              /* size of the physical display */
    int height;             /* size again */
    int depth;              /* number of colour planes */
    colour_t** clut;  		/* colour look up table (clut) */
	int	cidx;				// preallocated colours in the clut
   };

 struct window_blk
   {                                      /* info used to manage a window */ 
	int type;								/* our assigned type; parent (frame) or child */
	int dnum;                             /* associated display number */
	GC gc;                                /* graphics context */
	Window window;                        /* window handle */
	XdbeBackBuffer bbuffer;					// back buffer for double buffering
	int width;                            /* size in pixles of the window */
	int height;                           /* size again */
	int pressed;                          /* spring button pressed */
	int orig_x;								// translated origin
	int orig_y;
	double	scale;							// some things (buttons) are scaled
	XFontStruct *font;                    /* text font for the window */
	struct button_blk *blist;             /*  button blocks for the window */
	struct callback_blk cbs[LASTEvent];   /* possible callbacks for window */

	int	active_font;					// current font in use (for xft)
	int	active_colour;
   };
 
 struct bdef_blk            /* block used to add a block to the system */
  {
   int colour;               /* colour to paint the button in */
   char *text;              /* text to display on the button */
   int bid;                 /* button id */
   int type;                /* button type (spring, toggle etc) */
   int class;               /* class that the button belongs to */
   int options;             /* default options for the button */
  };

 struct button_blk          /* 3-d button information mgt block */
   {
    struct button_blk *next;  /* forward chain pointer */
    struct button_blk *prev;  /* backward chain pointer */
    int x;                    /* coords of upper left corner of button */
    int y;
    int height;               /* button height */
    int width;                /* button width */
    int type;                 /* button type (radio, toggle, spring) */
    int (*cbpress)();         /* pointer to press button callback */
    int (*cbrel)();           /* pointer to release button callback */
    void *data;               /* data pointer for callback routines */
    int bid;                  /* button id */
    int class;                /* button class */
    int flags;                /* status flags for the button */
    int colour;                /* colour to display button in */
    int tcolour;               /* colour to display button text */
    char *text;               /* string displayed on the button */
	int	font;				// the font id used to paint text on the button
   };

typedef struct {				// basic x font management (probably deprecated)
	XFontStruct* font;
	char* name;
} font_t;

typedef struct {					// management for xft fonts and text drawing
	XftDraw*	drawable;			// xft wraps the basic X display/window/colour in this
	XftFont*	font;
	char* name;
} xft_font_t;

#endif

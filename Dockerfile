#
# This docker file will create a build environment, and build the
# library.  The libraray can be extracted from the image, or the
# image can be used to build XI based applications.
#
# Build the image:
#	docker build -t libxi .
#

# Extract the library from the image with something like this:
#	docker run --rm -it  -v $PWD:/exfs --user $(id -u):$(id -g) image-name cp /usr/local/lib/libxi.a /exfs/
#
#	where "image-name" is the name given to  the image on build
#

FROM opensuse:latest

RUN zypper install -y freetype2-devel libX11-devel xorg-x11-server-extra  git gcc8 gcc8-c++ ksh gmake
RUN zypper install -y libXext-devel libXau-devel libXfixes-devel libXft-devel
RUN zypper install -y cmake
RUN ln -s /usr/bin/gcc-8 /usr/bin/gcc
ENV PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/bin:.

RUN mkdir /playpen && mkdir -p /usr/local/bin /usr/local/lib
WORKDIR /playpen

# build mk needed to build lib
#RUN cd /playpen && git clone https://github.com/ScottDaniels/mk.git &&  cd mk && PKG_ROOT=/usr/local ksh build

# force a rebuild if stale
COPY tag /playpen

# build the lib
#RUN cd /playpen && git clone https://bitbucket.org/EScottDaniels/xi.git && cd xi && mk && cp *.a /usr/local/lib

RUN git clone https://bitbucket.org/EScottDaniels/xi.git
RUN ls -al
RUN cd xi; mkdir .build; cd .build; cmake ..; make install

# silly command since this image really doesn't do anything
CMD [ "ls", "-al", "/usr/local/lib" ]


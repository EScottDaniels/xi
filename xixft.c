/*
=================================================================================================
	(c) Copyright 2018 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
--------------------------------------------------------------------------------------------------

	Mnemonic:	XIxft 
	Abstract:	XFT font management functions. This supports the xft font management
				API. For old core X11 fonts, and text drawing functions see the xifonts.c
				and xitext.c modules
	Date:		10 June 2018
	Author:	E. Scott Daniels

--------------------------------------------------------------------------------------------------
*/
#include "xiinclud.h"

static xft_font_t fonts[MAX_FONTS];
static int font_ip = 0;						// insertion point

// ------------------ private tools --------------------------------------------------------------

/*
static Region mk_region( int xo, int yo, int height, int width ) {
	XPoint	pts[4];

	pts[0].x = xo;
	pts[0].y = yo;
	pts[1].x = xo+width;
	pts[2].y = yo;
	pts[3].x = xo+width;
	pts[3].y = yo+height;
	pts[4].x = xo;
	pts[4].y = yo+height;

	return XPolygonRegion( pts, 4, EvenOddRule );
}
*/

// -----------------------------------------------------------------------------------------------

/*
	XIxft_find_font will search the current font list for the named font and return the font number
	that can be used on an XIuse_font() call. If the font is not known, it will be loaded. 
	This needs to do a search over the whole table; the user application really should save
	and map font numbers in a hash table, or manage the font number with the text.

	Styles are one of the XI_FS_* constants (bold/ital).

	If there is an error, XI_ERROR is returned.
*/
extern int XIxft_find_font( int wnum, char* font_family, int size, int style ) {
	int i;
	XftFont*	font;					// located font
	struct window_blk *wptr;			// window information
	XftPattern*	attempt_pat;			// pattern that we'll attempt to find
	XftPattern*	matched_pat;			// pattern matched
	XftResult	result;
	char	wbuf[1024];					// working buffer to build name in
	char*	sep = "";
	

	if( strcmp( font_family, "serif" ) == 0 || strcmp( font_family, "Serif" ) == 0 ) {
		font_family = "Times";
	} else {
		if( strncmp( font_family, "sans", 4 ) == 0 || strncmp( font_family, "Sans", 4 ) == 0 ) {
			font_family = "Helvetica";
		} else {
			if( strncmp( font_family, "fix", 3 ) == 0 || strncmp( font_family, "Fix", 3 ) == 0 ) {
				font_family = "Courier";
			}
		}
	}

	snprintf( wbuf, sizeof( wbuf ), "%s-%d%s", font_family, size, style ? ":" : "" );
	if( style & XI_FS_BOLD ) {
		strcat( wbuf, "weight=bold" );
		sep = ",";
	}
	if( style & XI_FS_ITAL ) {
		strcat( wbuf, sep );
		strcat( wbuf, "style=italic,oblique" );
	}

	for( i = 0; i < font_ip; i++ ) {					// if we've been down this road before, return the 'id'
		if( strcmp( wbuf, fonts[i].name ) == 0 ) {
			return i;
		}
	}

	if( font_ip >= MAX_FONTS ) {				// didn't find, and no room for more.
		return XI_ERROR;
	}

	if( (wptr = XIvalidw( wnum )) != NULL ) {
		attempt_pat = XftNameParse (wbuf );			// parse name into a pattern
		if( attempt_pat ) {
			matched_pat = XftFontMatch( dlist[wptr->dnum]->disp, dlist[wptr->dnum]->screen, attempt_pat, &result );		// find a suitible match
			font = XftFontOpenPattern( dlist[wptr->dnum]->disp, matched_pat );
		} else {
			fprintf( stderr, ">>> font failed to match: %s\n", wbuf );			
			return XI_ERROR;
		}

		if( font != NULL ) {
			fonts[font_ip].name = malloc_dup( wbuf );		// strdup isn't std, this is our flavour
			fonts[font_ip].font = font;
			font_ip++;;
			return font_ip - 1;
		}
		fprintf( stderr, ">>> ERR: unable to open font pattern for %s\n", wbuf );
	} else {
		fprintf( stderr, ">>> ERR: invalid window id\n" );
	}

	return XI_ERROR;
}


/*
	XIuse_font will activate the font with the given font number. If the number
	isn't valid, XI_ERROR is returned.  Activation involves only noting which 
	ID was indicated and thus repeated calls have on bearing on the display itself.
*/
extern int XIxft_use_font( int wnum, int font_id ) {
	struct window_blk *wptr;						// window data

	if( font_id < 0 || font_id >= font_ip ) {
		return XI_ERROR;
	}

	if( (wptr = XIvalidw( wnum )) != NULL ) {
		wptr->active_font = font_id;
		return XI_OK;
	}

	return XI_ERROR;
}
	
/*
	Get information about what it takes (space) to draw the string.
	Height and width of the bounding box returned using the pointers given
	(nil pointers are tolerated).  The return is true if the h/w values 
	were populated. Font_id may be XI_ACTIVE_FONT
*/
int XIxft_get_smetrics( int wnum, int font_id, char* string, int* height, int* width ) {
	XGlyphInfo extents;
	struct window_blk *wptr;

	if( (wptr = XIvalidw( wnum )) == NULL ) {
		return 0;
	}

	if( font_id == XI_ACTIVE_FONT ) {
		font_id = wptr->active_font;
	}

	XftTextExtents8 ( dlist[wptr->dnum]->disp, fonts[font_id].font,(FcChar8 *)string,strlen(string),&extents);
	if( height ) {
		*height = extents.height;
	}
	if( width ) {
		*height = extents.width;
	}

	return 1;
}

/*
	Draw the text (0 terminated; byte string) using the font with the given id. Text is 
	anchored (lower left) at xo,yo in the indicated window.  Colour_id may be any valid
	colour id, or XI_ACTIVE_COLOUR to use the currently active colour.
*/
extern int XIxft_draw_text( int wnum, int xo, int yo,  int colour_id, int font_id, char* string, int opts ) {
	struct window_blk *wptr;			// window data
	XftDraw* ctxt;						// drawing context
	XftColor colour;
	XRenderColor xrc;
	Drawable target;

	if( (wptr = XIvalidw( wnum )) == NULL ) {
		return XI_ERROR;
	}

	if( font_id == XI_ACTIVE_FONT ) {
		font_id = wptr->active_font;
	}

	if( opts & XI_OPT_BB ) {				// select target based on options
		target = wptr->bbuffer;
	} else {
		target = wptr->window;
	}

	xo += wptr->orig_x;
	yo += wptr->orig_y;
 
	ctxt = XftDrawCreate( dlist[wptr->dnum]->disp, target, dlist[wptr->dnum]->visual, syscolours );
	XIget_colour_rgb( wnum, colour_id, &xrc.red, &xrc.green, &xrc.blue );
	xrc.alpha = 0xffff;

	XftColorAllocValue( dlist[wptr->dnum]->disp, dlist[wptr->dnum]->visual,  syscolours, &xrc, &colour );

	XftDrawString8( ctxt, &colour, fonts[font_id].font, xo, yo, (XftChar8 *) string, strlen( string ) );

	XFlush( dlist[wptr->dnum]->disp );

	XftDrawDestroy( ctxt );

	return XI_OK;
}


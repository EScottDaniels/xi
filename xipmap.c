/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
*
* Mnemonic: XIpmap
* Abstract:	Manage a pix map for things like double buffering
*
*		This routine is responsible for drawing an image into the window;
* Date:     28 Feb 2010
* Author:   E. Scott Daniels
*
****************************************************************************
*/
#include "xiinclud.h"

typedef struct XIpxmap {
	struct window_blk *wptr;		/* caller need only supply ptr to this if we keep these bits */
	struct display_blk *disp;
	int	nalloc;		/* number allocated */
	int	idx;		/* current pxmap that is addressed */
	Pixmap	*pxmaps;	/* list of pix maps */
} XIpxmap;



extern XIpxmap *XIpm_new( int window, int height, int width,  int n2alloc )
{
 	struct window_blk *wptr;    	/* pointer at our window information  for the window */
	struct display_blk *display;
	XIpxmap *pptr;
        //Visual *visual; 
	int	depth;			/* display depth */
	//int	x;
	//int	y;
	int 	i;
	//char	*xpxmap;		/* destination for conversion of rgb/a data */
	//char	*xpptr;			/* pointer into the xpxmap */
	//unsigned char	*pxp;		/* pointer into source pxmap */
	//unsigned  long	p = 0;		
	//char 	*nxt;			/* pointer at next row in source */

 	if( (wptr = XIvalidw( window )) == NULL )
  		return( NULL );

	display = dlist[wptr->dnum];
	//visual = DefaultVisual( display->disp, display->screen );
        depth = DefaultDepth( display->disp, display->screen ); 


	pptr = (XIpxmap *) malloc( sizeof( XIpxmap ) );
	if( pptr == NULL )
		return pptr;

	pptr->nalloc = n2alloc;
	pptr->pxmaps = (Pixmap *) malloc( sizeof( Pixmap ) * n2alloc );
	if( pptr->pxmaps == NULL )
	{
		free( pptr );
		return NULL;
	}

	pptr->wptr = wptr;
	pptr->disp = display;
	for( i = 0; i < n2alloc; i ++ )
		pptr->pxmaps[i] = XCreatePixmap( pptr->disp->disp, wptr->window, width, height, depth );

	pptr->idx = 0;

	return pptr;
}

/* select one; select next if which is < 0 */
extern void XIpm_select( XIpxmap *pptr, int which  )
{
	if( which == XI_OPT_NEXT || which > pptr->nalloc )
	{
		if( (++pptr->idx) >= pptr->nalloc )
			pptr->idx = 0;
	}
	else
		pptr->idx = which;
}

/* draws the string into the currently selected pixmap */
extern void XIpm_drawtext( XIpxmap *pptr, int x, int y, char *txt )
{
	XDrawImageString( pptr->disp->disp, pptr->pxmaps[pptr->idx], pptr->wptr->gc, x, y, txt, strlen( txt ) );
}

/* add the image to the pxmap 
	sx/y are source x and y
	dx/y... are dest x and y
*/
extern void XIpm_drawimage(  XIpxmap *pptr, XImage *image, int sx, int sy, int dx, int dy, unsigned int h, unsigned int w )
{
	XPutImage( pptr->disp->disp, pptr->pxmaps[pptr->idx], pptr->wptr->gc, image, sx, sy, dx, dy, w, h );
}
	

/* draw the pix map into the window */
extern void XIpm_draw( XIpxmap *pptr, int sx, int sy, int dx, int dy, unsigned int h, unsigned int w, int opts )
{
int e;

	e = XCopyArea( pptr->disp->disp, pptr->pxmaps[pptr->idx], pptr->wptr->window, pptr->wptr->gc, sx, sy, w, h, dx, dy );
fprintf( stderr, "e=%d\n", e );

	XFlush( pptr->disp->disp );       

	if( opts == XI_OPT_NEXT ) 		/* automatically select the next one */
		XIpm_select( pptr, XI_OPT_NEXT );

}

extern void XIpm_free( XIpxmap *pptr )
{
	int i;

	for( i = 0; i < pptr->nalloc; i++ )
	{
		XFreePixmap(pptr->disp->disp, pptr->pxmaps[i] );
		free( pptr->pxmaps );	
		free( pptr );
	}
}

/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
*****************************************************************************
*
*  Mnemonic: XIpoll
*  Abstract: This routine will check pending events for a display and 
*            return the next event if there are any pending. If there are
*            no events pending nothing occurs (the process does not block).
*  Parms:    parent - The display number 
*            event  - Pointer to the event block that will be filled in if
*                     an event was pending
*            ewin   - Pointer to integer to be filled in with the window
*                     number that took the event.
*  Returns:  Number of events in the queue PRIOR to processing the event
*            returned (thus a return of 0 indicate no event was waiting).
*  Date:     8 January 1997
*  Author:   E. Scott Daniels
*
*****************************************************************************
*/
#include "xiinclud.h"

int XIpoll( int parent, XEvent *event, int *ewin )
{
	//struct display_blk *dptr;       /* pointer at display info for pending call */
	struct window_blk *wptr;        /* window info related to num passed in */
	int count = 0;                  /* events in the queue */

	if( (wptr = XIvalidw( parent)) != NULL )   /* valid window passed in */
	{
		if( (count = XPending( dlist[wptr->dnum]->disp )) )  /* if events pending */
			*ewin = XIevent( parent, event );   		/* then get it */ 
	}                                        /* end if valid window number */ 

 	return count;
}                      /* XIpoll */

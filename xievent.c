/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
**** DEPRECATED ******




****************************************************************************
*
* Mnemonic: XIevent
* Abstract: This routine is responsible for getting the next event for a 
*            given display and returning the information to the caller.
* Parms:    parent - Window number of a parent window in the display.
*           event - pointer to an event structure
* Date:     9 September 1991 
* Author:   E. Scott Daniels
*
* NOTES: This routine is now obsolete with the addition of the XIdriver.
*        It remains a part of the library for back level compatability with
*        any existing applications that may reference it, but it should not
*        be used in any new applicaitons, and applications undergoing maint
*        should be modifed to use the driver and callbacks as it will make 
*        the application straighforward.
****************************************************************************
*/
#include "xiinclud.h"

extern int XIevent( int parent, XEvent *event)
{
	struct window_blk *wptr;    /* pointer at the window information */
	//int status;                 /* processing status */
	int wnum;                   /* index into window list */
	Window ewindow;             /* window event took place in */

	if( (wptr = XIvalidw( parent )) == NULL )
		return XI_ERR_BADWNUM;

	XNextEvent( dlist[wptr->dnum]->disp,  event );
	
	ewindow = event->xany.window;
	switch( event->xany.type )
	{
		case  KeyPress:
		case  KeyRelease:
			if( event->xkey.subwindow != 0 )	
				ewindow = event->xkey.subwindow;
			break;
		case  ButtonPress:
		case  ButtonRelease:
			if( event->xbutton.subwindow != 0 )	
				ewindow = event->xbutton.subwindow;
			break;
		case  MotionNotify:
			if( event->xmotion.subwindow != 0 )	
				ewindow = event->xmotion.subwindow;
			break;
		case  EnterNotify:
			break;
		case  LeaveNotify:
			break;
		case  FocusIn:
			break;
		case  FocusOut:
			break;
		case  KeymapNotify:
			break;
		case  Expose:
			break;
		case  GraphicsExpose:
			break;
		case  NoExpose:
			break;
		case  VisibilityNotify:
			break;
		case  CreateNotify:
			break;
		case  DestroyNotify:
			break;
		case  UnmapNotify:
			break;
		case  MapNotify:
			break;
		case  MapRequest:
			break;
		case  ReparentNotify:
			break;
		case  ConfigureNotify:
			break;
		case  ConfigureRequest:
			break;
		case  GravityNotify:
			break;
		case  ResizeRequest:
			break;
		case  CirculateNotify:
			break;
		case  CirculateRequest:
			break;
		case  PropertyNotify:
			break;
		case  SelectionClear:
			break;
		case  SelectionRequest:
			break;
		case  SelectionNotify:
			break;
		case  ColormapNotify:
			break;
		case  ClientMessage:
			break;
		case  MappingNotify:
			break;

		default:
			break;
	}

	for( wnum = 0; wnum < MAX_WINDOWS; wnum++ )  				/* search for our window info */
		if( (wptr = XIvalidw( wnum )) != NULL && wptr->window == ewindow )
			return wnum;

	return ERROR;          /* event not in a valid window */
}
                              /* XIevent */

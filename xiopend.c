/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XIopend
* Abstract: This routine is responsible for opening a display.
* Parms:    dname - pointer to the name of the display.
* Returns:  display handle (display number) or XI_ERR error constant
* Date:     25 July 1991 (PORT FOM VMS)
* Author:   E. Scott Daniels
*
* Mods:		16 Jun 2018 - better formatting; modern prototype.
*
****************************************************************************
*/
#include "xiinclud.h"

extern int XIopend( char* dname ) {
	int dnum;                  /* index into dlist array for this display */
	struct display_blk *dptr;  /* pointer to the display block allocated */

	for( dnum = 0; dnum < MAX_DISPLAY && dlist[dnum] != NULL; dnum++ );
	if( dnum >= MAX_DISPLAY ) {				// too many open
		return XI_ERR_MAXOPEN;
	}

	dlist[dnum] = dptr = (struct display_blk *) malloc(sizeof(struct display_blk));
	if( dptr == NULL ) {
		return( XI_ERR_NOMEM );
	}
	memset( dptr, 0, sizeof( *dptr ) );

	dptr->disp = XOpenDisplay( dname );  			// get reference to the display
	if( dptr->disp == NULL ) {
		free( dptr );
		dlist[dnum] = NULL;
		return XI_ERR_DOPEN;
	}

	dptr->clut = (colour_t **) malloc( sizeof( colour_t* ) * MAX_COLOURS * 3 );
	if( dptr->clut == NULL ) {
		fprintf( stderr, "memory allocation error: clut\n" );
		exit( 2 );
	}

 	/* now that we've got the display lets get some basic info about it */

	dptr->screen = DefaultScreen( dptr->disp );
	dptr->visual = DefaultVisual( dptr->disp, dptr->screen );		// visual for the default screen
	dptr->width = DisplayWidth( dptr->disp, dptr->screen );
	dptr->height = DisplayHeight( dptr->disp, dptr->screen );
	dptr->depth = DefaultDepth( dptr->disp, dptr->screen );
	
	XIloadcolour( dptr );   /* load the color look up table for this display */
	
	XFlush( dptr->disp );  /* dump off anything that is pending */
	
	return dnum;
}

/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
*
*  Mnemonic: 	XIbtnmgr
*  Abstract: 	This routine will manage a screen button: determining current 
*            	status, making the proper call back, and changing the display 
*            	if necessary. This function is called by the user programme
*				with the event to determine if a screen button was 
*				under the pointer when the mouse button was clicked/released.
*  Parms:    wid  - The window number that the event occured in 
*            event- X event block describing the event that occured
*  Returns:  Result of the callback (int) or bid of screen button if no cb 
*            for the button; XI_ERR_NOBUTTON if press was not on a screen
*            button. 
*  Date:     30 December 1996
*  Author:   E. Scott Daniels
*
*****************************************************************************
*/
#include "xiinclud.h"

extern int XIbtnmgr( int wnum, XButtonEvent *event )
{
 int bid;                         /* id of the button pressed */
 int status = XI_OK;              /* status to return to caller */
 struct button_blk *bptr = NULL;  /* pointer at the button management block */
 struct button_blk *rptr = NULL;  /* pointer at the current radio button */
 struct window_blk *wptr;         /* ptr at window inwhich event occured */
 
 if( (wptr = XIvalidw( wnum )) == NULL )   /* window number rep a good one? */
  return( XI_ERR_NOBUTTON );

 bid = XIfindbutton( wnum, event->x, event->y );  /* was a button pressed? */

 if( bid != XI_ERR_NOBUTTON )      /* screen button was found in window */
  {                                 
   status = bid;
   for( bptr=wptr->blist; bptr != NULL && bptr->bid != bid; bptr = bptr->next);
   if( event->type  == ButtonPress )   /* user pressed mouse button */
    {
     switch( bptr->type )   /* act based on button type */
      {
       case XI_RADIO_BUTTON:    
         rptr = wptr->blist;
         while( rptr != NULL &&
            !( rptr->class == bptr->class && (rptr->flags & BF_PRESSED) ) )
            rptr = rptr->next;
         if( rptr != NULL )               /* found one pressed in */
          {
			if( rptr->cbrel != NULL )      /* call release cb routine */
				status = (rptr->cbrel)( bid, rptr->class, event, rptr->data);
           
            XImodbutton( wnum, 0, 0, 0, 0, NULL, NULL,  0, 0, NULL, rptr->bid, 0, XI_MOD_RELEASE );    /* raise button */
            /*rptr->flags &= ~BF_PRESSED; */
          }
                                                 /* now do button clicked on */
         XImodbutton( wnum, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, bid, 0, XI_MOD_PRESS );   /* paint depressed but */

         if( bptr->cbpress != NULL )
          status = (bptr->cbpress)( bid, bptr->class, event, bptr->data);
         /*bptr->flags |= BF_PRESSED; */
         break;

       case XI_TOGGLE_BUTTON:
		if( !(bptr->flags & BF_PRESSED) )      /* if not pressed already */
		{                                     /* press, lock, make callback*/
			XImodbutton( wnum, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, bid, 0, XI_MOD_PRESS | XI_MOD_LOCK );
			bptr->flags |= BF_PRESSED | BF_LOCKED; /* set flag to"lock"in */
			if( bptr->cbpress != NULL )
				status = (bptr->cbpress)( bid, bptr->class, event, bptr->data);
			
		}
		else
		{
			XImodbutton( wnum, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, bid, 0, XI_MOD_UNLOCK );  
			bptr->flags &= ~BF_LOCKED;     /* unlock so release pops it */
#ifdef KEEP
			if( bptr->cbrel != NULL )
				status = (bptr->cbrel)( bid, bptr->class, event, bptr->data);
#endif
		}

		wptr->pressed = -1;
		break;

       case XI_SPRING_BUTTON:
         XImodbutton(wnum, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, bid, 0, XI_MOD_PRESS );
         wptr->pressed = bid;                         /* save pressed id */
         if( bptr->cbpress != NULL )
          status = (bptr->cbpress)( bid, bptr->class, event, bptr->data);
         break;

       case XI_ENTRY_BUTTON:             /* no action on button press */
         break;
 
       default:
         break;
      }                      /* end button type switch */   
    } 
   else                  /* else - assume mouse button was released */
    {
     if( wptr->pressed != XI_ERROR && wptr->pressed != bid )
      {                                       /* mouse not on spring btn */
       XImodbutton( wnum, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, wptr->pressed, 0, XI_MOD_RELEASE );
       wptr->pressed = XI_ERROR;     
      }
     else     /* cursor on active spring button or valid release */
      {
       switch( bptr->type )   /* act based on button type */
        {
         case XI_RADIO_BUTTON:    /* no action on a release event as release */
           break;              /* callback driven when other is pressed */
  
         case XI_SPRING_BUTTON:
           if( wptr->pressed == bid )           /* if still on the button */
            if( bptr->cbrel != NULL )
           	status = (bptr->cbrel)( bid, bptr->class, event, bptr->data);
           XImodbutton( wnum, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, wptr->pressed, 0, XI_MOD_RELEASE );
           wptr->pressed = XI_ERROR;    
           break;

         case XI_TOGGLE_BUTTON:
           if( !(bptr->flags & BF_LOCKED ) )   			/* if not locked in */
            {
             	XImodbutton(wnum, 0,0,0,0, NULL, NULL, 0, 0,  NULL, bid, 0, XI_MOD_RELEASE );
		bptr->flags &= ~BF_PRESSED;
             	if( bptr->cbrel != NULL )
              		status = (bptr->cbrel)( bid, bptr->class, event, bptr->data);
            }
           break;
  
         case XI_ENTRY_BUTTON:           /* set focus to this button */
  
         default:
           break;
        }                      /* end button type switch */   
      }                  /* end else valid release */
    }                    /* end user released mouse button */
  }                      /* end if mouse event on a screen button */
 else          /* button press/release not on a screen button */
  {
   if( wptr->pressed != XI_ERROR )   /* if a button was pressed earlier */
    {                                                /* then release it */
     XImodbutton( wnum, 0, 0, 0, 0, NULL,  NULL, 0, 0,  NULL, wptr->pressed, 0, XI_MOD_RELEASE );
     wptr->pressed = XI_ERROR;     
    }
   status = XI_ERR_NOBUTTON;  /* send back an error code */
  }

 return( status );       /* return the status to the caller */
}

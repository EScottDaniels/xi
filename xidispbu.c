/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XIdispbutton
* Abstract: This routine will draw one or all of the buttons in the button
*           list attached to a window block.
* Parms:    wnum - window number (index into xiwins.)
*           bid  - Id or class of the button(s) to display
*           option- Draw all, all in a class, or one button
* Date:     12 September 1991
* Author:   E. Scott Daniels
*
* Modified: 27 Dec 1996 - To support displaying a pressed button
*			20 Jun 2018 - Set xft font for the button
****************************************************************************
*/
#include "xiinclud.h"

extern int XIdispbutton( wnum, bid, option )
 int wnum;
 int bid;
 int option;
{
 struct window_blk *wptr;    /* pointer at window information block */
 struct button_blk *bptr;    /* pointer into the button list */
 int doption;                /* option to pass to xidrawb */

 if( (wptr = XIvalidw( wnum )) == NULL )
  return( XI_ERR_BADWNUM );    /* let them know they blew it */

 if( option & XI_OPT_ONE )   /* search for the button with the matching bid */
  {
   for( bptr = wptr->blist; bptr != NULL && bptr->bid != bid; bptr=bptr->next);
   if( bptr != NULL )      /* found the button description block */
    {
		doption = (bptr->flags & BF_BACK) ? XI_OPT_BACKGROUND : 0; 		// set display options based on button's flags 
		doption |= (bptr->flags & BF_PRESSED) ? XI_OPT_IN : 0;
		XIxft_use_font( wnum, bptr->font );
		XIdrawb( wnum, bptr->x, bptr->y, bptr->height, bptr->width, bptr->colour, bptr->tcolour,  bptr->text, doption );
		bptr->flags = bptr->flags | BF_PAINTED;   /* indicate that its visible */
    }
  }      /* end if paint only 1 */
 else
  if( option & XI_OPT_CLASS )     /* paint all buttons of the defined class */
   {
    for( bptr = wptr->blist; bptr != NULL;  bptr = bptr->next ) /* run list */
     {
      if( (bptr->flags & BF_DISPLAY)  &&  (bptr->class == bid ) )
       {
                            /* set display options based on button's flags */
        doption = (bptr->flags & BF_BACK) ? XI_OPT_BACKGROUND : 0;
        doption |= (bptr->flags & BF_PRESSED) ? XI_OPT_IN : 0;
        doption |= option & XI_OPT_BB;

        XIdrawb(wnum, bptr->x, bptr->y, bptr->height, bptr->width, bptr->colour, bptr->tcolour, bptr->text, doption );
        bptr->flags = bptr->flags | BF_PAINTED;   /* set visible flag */
       }                                          /* end if displayable */
     }                                            /* end for buttons */
   }           /* end if painting all from a class */
  else         /* assume we are painting all buttons with display flag set */
   {
    for( bptr = wptr->blist; bptr != NULL;  bptr = bptr->next ) /* run list */
     {
      if( bptr->flags & BF_DISPLAY )   /* if allowed to display button */
       {
                            /* set display options based on button's flags */
        doption = (bptr->flags & BF_BACK) ? XI_OPT_BACKGROUND : 0;
        doption |= (bptr->flags & BF_PRESSED) ? XI_OPT_IN : 0;

        XIdrawb(wnum, bptr->x, bptr->y, bptr->height, bptr->width, bptr->colour, bptr->tcolour, bptr->text, doption );
        bptr->flags = bptr->flags | BF_PAINTED;   /* set visible flag */
       }                                          /* end if displayable */
     }                                            /* end for buttons */
   }                                              /* end else do all */

 if( option & XI_OPT_FLUSH )       /* caller wants display flushed */
  XFlush( dlist[wptr->dnum]->disp );  /* so flush it */

 return( XI_OK );
}                   /* XIdispbutton */

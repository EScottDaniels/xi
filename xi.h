/*
=================================================================================================
	(c) Copyright 1995-2018 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/

/*
------------------------------------------------------------------------------
*  xi constants and structure definitions needed by user code
------------------------------------------------------------------------------
*/

#ifndef _xi_h

#define XI_OPT_ALL        0       /* process all in the list */
#define XI_NOOPTION       0       /* no option supplied on function */
#define XI_OPT_NOFILL     0       /* dont fill the object */
#define XI_OPT_FILL       0x01    /* fill the object */
#define XI_OPT_FLUSH      0x02    /* flush the queue to window manager */
#define XI_OPT_BACKGROUND 0x04    /* supply background with button */
#define XI_OPT_ONE        0x08    /* process only one element in list */
#define XI_OPT_HIDE       0x10    /* mark the button as hidden */
#define XI_OPT_CLASS      0x20    /* apply process to all class members */
#define XI_OPT_CALL       0x40    /* invoke call back if indicated */
#define XI_OPT_IN         0x80    /* display button pressed in */
#define XI_OPT_FOCUS      0x100   /* grab focus for the window */
#define XI_OPT_BB			0x200 // write/draw to the back buffer
#define XI_OPT_NEXT	-1		/* next whatever */

				/* various callback return values */
#define XI_RET_QUIT       (-1)  /* signals xidriver to stop and return control to user code */  

                                  /* driver modes */
#define XI_MODE_NORMAL    1       /* normal blocking mode */
#define XI_MODE_POLL      2       /* poll mode */

                                  /* button modification options */
#define XI_MOD_TEXT    0x01       /* update the text for the button */
#define XI_MOD_SHOW    0x02       /* flag the button as displayable */
#define XI_MOD_CALL    0x04       /* modify the callback rtn address */
#define XI_MOD_CLASS   0x08       /* modify the class of the button */
#define XI_MOD_HIDE    0x10       /* erase the button (keep in list) */
#define XI_MOD_COLOR   0x20       /* change the color of the button */
#define XI_MOD_REL     0x40       /* change xy and hw relative to cur vals */
#define XI_MOD_HW      0x80       /* change the height and width of button */
#define XI_MOD_XY      0x100      /* change the location of the button */
#define XI_MOD_BID     0x200      /* change the button id for the button */
#define XI_MOD_DEL     0x400      /* delete the button from list and erase */
#define XI_MOD_PRESS   0x800      /* display pressed */
#define XI_MOD_RELEASE 0x1000     /* display button released (popped out) */
#define XI_MOD_LOCK    0x2000     /* lock the button down */
#define XI_MOD_UNLOCK  0x4000     /* unlock the button */

#define XI_KEYPRESS	KeyPressMask                    	/* help keep user unaware of X consts */
#define XI_KEYRELEASE	KeyReleaseMask                  
#define XI_BUTTONPRESS	ButtonPressMask                 
#define XI_BUTTONRELEASE	ButtonReleaseMask               
#define XI_ENTERWINDOE	EnterWindowMask                 
#define XI_LEAVEWINDOW	LeaveWindowMask                 
#define XI_POINTMOTION	PointerMotionMask               
#define XI_MOTIONB1	Button1MotionMask 		
#define XI_MOTIONB2	Button2MotionMask 
#define XI_MOTIONB3	Button3MotionMask
#define XI_MOTIONB4	Button4MotionMask              
#define XI_MOTIONB5	Button5MotionMask             
#define XI_BUTTONMOTION	ButtonMotionMask             
#define XI_EXPOSURE	ExposureMask                
#define XI_VISCHANGE	VisibilityChangeMask       
#define XI_STRUCTURENOTIFY StructureNotifyMask       
#define XI_RESIZE	ResizeRedirectMask       
#define XI_FOCUSCHANGE	FocusChangeMask         
#define XI_CONFIGURENOTIFY ConfigureNotifyMask
#define XI_PROPERTYNOTIFY PropertyChangeMask

#define XI_POLL_EVENTS	KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask | EnterWindowMask | LeaveWindowMask | PointerMotionMask | Button1MotionMask | Button2MotionMask | Button3MotionMask | Button4MotionMask | Button5MotionMask | ButtonMotionMask | ExposureMask | VisibilityChangeMask | StructureNotifyMask | ResizeRedirectMask | FocusChangeMask |  PropertyChangeMask


#define XI_EV_STRUCTCHANGE	ConfigureNotify


                           	/* button types */
#define XI_RADIO_BUTTON   1	/* radio button class member - 1 pressed */
#define XI_TOGGLE_BUTTON  2	/* button sticks until pressed again */
#define XI_SPRING_BUTTON  3	/* button springs up - never stays in */
#define XI_ENTRY_BUTTON   4	/* text entry "button" */

#define XI_PARENT      0          /* window will be a child of the screen */
#define XI_CHILD       1          /* window will be child of existing win */
#define XI_BORDERLESS_PARENT 2	  /* child of screen, but with no frame */
#define XI_BORDERLESS_CHILD 3	  /* same as child but border size of 0 */

#define XI_FS_NORMAL	0x00	// font options for style
#define XI_FS_BOLD		0x01		
#define XI_FS_ITAL		0x02

#define XERROR            0       /* error indicator returned by xlib */
#define XI_OK             0       /* processing successful */
#define XI_ERROR          (-1)    /* general error condition */
#define XI_ERR_NOMEM      (-2)    /* unable to get storage */
#define XI_ERR_MAXOPEN    (-3)    /* max num of winodws already opened */
#define XI_ERR_DOPEN      (-4)    /* unable to open the display */
#define XI_ERR_WOPEN      (-5)    /* unable to open a window */
#define XI_ERR_BADWNUM    (-6)    /* invalid window num passed in */
#define XI_ERR_BADDNUM    (-7)    /* invalid display number passed in */
#define XI_ERR_BADVAL     (-8)    /* bad value passed in */
#define XI_ERR_BADPARENT  (-9)    /* parent id invalid */
#define XI_WRN_DUPID      (-10)   /* duplicate id */
#define XI_ERR_NOBUTTON   (-11)   /* no button pressed */
#define XI_ERR_BADID      (-12)   /* bad id passed */
#define XI_ERR_BADEVENT   (-13)   /* event number not in range of X events */


  /* color lookup table constants */
  /* color lookup table (clut) supports MAX_COLOURS colors, and 2 variations on each 
     color (dark and light)  The first MAX_COLOURS  entries in the clut are 
     the "normal" color, the second MAX_COLOURS  are the "light" version of the 
     color and the third set are the "dark" version of the color. Colors
     can be passed into the XI routines as: BLUE or BLUE+LIGHT or BLUE+DARK

	when users are defining their own colours, they need only define the 
	normal r/g/b; light and dark versions will automatically be calculated. 
	light and dark variations of the colours are mostly used for shadowing
	and highlighting for 3d effect. 

	these colours map to the colour names in the array in xiloadcolour.c

	user programmes can add/override the clut using xidefcolour
  */

#define XI_NORMAL			0					// probably internal options when defining a colour
#define XI_DARKEN			1
#define XI_LIGHTEN			2
#define XI_LIGHT           MAX_COLOURS        	// add to colour index value (e.g. XI_BLUE + XI+DARK) to get a lighter/darker version of the colour
#define XI_DARK            (MAX_COLOURS * 2)
#define XI_ACTIVE_COLOUR	(-1)				// value to force the active colour to be selected
#define XI_ACTIVE_FONT		(-1)				// value to force the active font to be selected

#include "xicolours.h"		// colour constants generated by build process

#include "xistruct.h"
#include "xiproto.h"

#define _xi_h
#endif

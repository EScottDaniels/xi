/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
*
*  Mnemonic: XIprompt  (xiprompt.c)
*  Abstract: This routine will setup a prompt window on the scrren and 
*            register the necessary callback routines to handle events
*            on the window. The colour of the window is taken from the 
*            first button in the button list. 
*  Parms:    parent  - Parent window handle to draw this prompt in 
*            text    - Prompt text to display
*            buf     - Buffer to allow user to fill in (Null if no edit)
*            blen    - Length of buffer to fill 
*            blist   - Pointer at button definion block list to display
*            cbrtn   - Address of routine to call when prompt closes
*            cbdata  - Pointer to pass to callback routine
*  Returns:  XI_OK if all was ok, XI_ERROR if prompt could not be initiated.
*  Date:     23 January 1997
*  Author:   E. Scott Daniels
*
****************************************************************************
*/
#include "xiinclud.h"

int XIprompt( int parent, char *text, char *buf, int blen, struct bdef_blk *blist, int ((*cbrtn)()), void *cbdata )
{
 int status = XI_ERROR;         /* error processing here somewhere */
 int i;                         /* loop index */
 int colour;                     /* colour to make the window */
 struct prompt_blk *pptr;       /* new prompt block */
 struct bdef_blk *bptr;         /* button block list pointer */

 if((pptr = (struct prompt_blk *) malloc( sizeof(struct prompt_blk))) != NULL )
  {
   colour = blist[0].colour;

   pptr->window = XIopenw( parent, XI_CHILD, 10, 10, 350, 150, XI_WHITE, colour, NULL,  FALSE );
   pptr->cbdata = cbdata; 
   pptr->cbrtn = cbrtn;
   pptr->fcolor = XI_WHITE; 
   pptr->bcolor = colour;
   pptr->text = (char *) malloc( strlen( text ) + 1 );
   strcpy( pptr->text, text );     /* save text for repaint */

   if( pptr->window >= XI_OK  )
    {
     if( buf != NULL )     /* need to show an edit area and setup edit? */
      {
       pptr->eptr = (struct edit_blk *) malloc( sizeof( struct edit_blk ) );
       if( pptr->eptr != NULL )
        {
         pptr->eptr->window = XIopenw( pptr->window, XI_CHILD, 5, 60, 330, 25, colour, colour, NULL,  FALSE );
         if( pptr->eptr->window >= XI_OK )
          {
           XIaddbutton( pptr->eptr->window, 2, 2, 20, 325, NULL, NULL, NULL, colour, XI_WHITE, " ", ERROR, ERROR, ERROR, XI_OPT_FLUSH | XI_OPT_IN );
           pptr->eptr->buf = buf;        
           pptr->eptr->idx = 0;
           pptr->eptr->didx = 0;           
           pptr->eptr->blen = blen;
           pptr->eptr->cwidth = XIstrlen( pptr->eptr->window, " " );
/*
           pptr->eptr->cwidth = 7;
*/
           pptr->eptr->cheight = 10;
           pptr->eptr->fcolor = XI_WHITE;
           pptr->eptr->bcolor = colour + XI_DARK;
           pptr->eptr->wwidth = 320;           /* width of the window */
           pptr->eptr->cbrtn = &XIpmgr;
           pptr->eptr->buf[0] = EOS;            /* terminate buffer */

           XIecbreg( pptr->eptr->window, KeyPress, &XIemgr, pptr->eptr );
           XIecbreg( pptr->eptr->window, FocusIn,  &XIemgr, pptr->eptr );
           XIecbreg( pptr->eptr->window, FocusOut, &XIemgr, pptr->eptr );
           XIecbreg( pptr->eptr->window, Expose,   &XIemgr, pptr->eptr );
           XIecbreg( pptr->eptr->window, ButtonPress, &XIemgr, pptr->eptr );

           XIfocus( pptr->eptr->window );    /* grab focus to the window */
          }
         else
          {
           free( pptr->eptr );
           pptr->eptr = NULL;
          }
        }
      }

     XIsetcolour( pptr->window, XI_WHITE );      /* set colour for prompt str */
     XIdrawtext( pptr->window, 5, 12, text );   /* show prompt string */

     for( i = 0, bptr = blist; bptr->class != ERROR; bptr++, i++ )
      XIaddbutton( pptr->window, (i * 53) + 15, 120, 25, 50, 
                   NULL, NULL, pptr,
                   colour, 
			XI_WHITE, 
                   bptr->text,
                   bptr->bid,
                   bptr->type,
                   bptr->class,
                   bptr->options );       /* add button to window */

     /* register the prompt manager routine as the event callback routine */
     XIcbreg( pptr->window, ButtonPress, &XIpmgr, pptr );
     XIcbreg( pptr->window, ButtonRelease, &XIpmgr, pptr );
     XIcbreg( pptr->window, Expose, &XIpmgr, pptr );

     status = XI_OK;                      /* all ok so let caller know */
                       
   }               /* end if window opened ok */
  }                /* end block allocated ok */

 return( status );
}                               /* XIprompt */

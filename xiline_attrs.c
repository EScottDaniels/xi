/*
=================================================================================================
	(c) Copyright 2018 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/

/*
****************************************************************************
*
* Mnemonic: XIline_attrs
* Abstract: Manage line attributes
* Date:     22 July 2018
* Author:   E. Scott Daniels
*
*	Mods:
*
****************************************************************************
*/
#include "xiinclud.h"

/*
	Set the various line atrributes in the gc.  Dashed is boolean (dashed or not).
	Cap and join sytles are ignored at the moment.
*/
#define USED(a) a=a
extern int XIline_attrs( int window, int dashed, int width, int cap_style, int join_style ) { 
	struct window_blk *wptr;    /* pointer at the window information */

	if( (wptr = XIvalidw( window )) == NULL )
		return( XI_ERR_BADWNUM );

	USED( cap_style );
	USED( join_style );
	
	XSetLineAttributes( dlist[wptr->dnum]->disp, wptr->gc, width, dashed ? LineOnOffDash : LineSolid, CapButt, JoinRound );

	return XI_OK;
}   

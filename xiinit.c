/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XIinit
* Abstract: This routine is responsible for the initialization of the XI 
*           runtime toolkit.
* Parms:    None.
* Returns:  XI_OK
* Date:     25 July 1991 (PORT FOM VMS)
* Author:   E. Scott Daniels
*
****************************************************************************
*/
#define INTERNAL            /* define global variables here (not externs) */

#include "xiinclud.h"

static int silent_errs = 1;

int errh( Display *d, XErrorEvent *e )
{
	char buf[1024];

	if( silent_errs )
		return 0;

	XGetErrorText( d, e->error_code, buf, sizeof( buf ) );
	fprintf( stderr, "error trapped: %s\n", buf );
	return 0;
}

int ioerrh( Display *d )
{
	if( silent_errs )
		return 0;

	fprintf( stderr, "I/O error trapped: %p\n",  (void *) d );
	return 0;
}

void XIset_err_display( int how )
{
	silent_errs = how; 
}

extern int XIinit( )
{

	memset( dlist, 0, sizeof( dlist ) );
	memset( xiwins, 0, sizeof( xiwins ) );

	XInitThreads();
	XSetErrorHandler( errh );
	XSetIOErrorHandler( ioerrh );

	return XI_OK;
}             

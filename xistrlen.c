/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
***************************************************************************
*
*  Mnemonic: xistrlen (xistrlen.c)
*  Abstract: This routine will return the length (in pixles) required
*            to represent the string in the specified window.
*  Parms:    wnum - Window number to use
*            str  - String to check
*  Returns:  Integer value of the pixles required to represent the 
*            string.
*  Date:     27 January 1997
*  Author:   E. Scott Daniels
*  
****************************************************************************
*/
#include <stdio.h>
#include "xiinclud.h"

int XIstrlen( int wnum, char *str )
{
 struct window_blk *wptr;          /* pointer at window block to use */
 int rval = ERROR;                 /* default return value to error */

 if( (wptr = XIvalidw( wnum )) != NULL )   /* if valid window passed in */
  {
   if( wptr->font != XERROR )
    rval = XTextWidth( wptr->font, str, strlen( str ) );    
   else
    {
 fprintf( stderr, "xistrlen: guessing at string width; ->font is XERROR\n" );
     rval = 6 * strlen( str );
    }
  }

 return( rval );         /* let caller know result */
}                  /* XIstrlen */

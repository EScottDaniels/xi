/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XIloadcolour 
* Abstract: This routine is responsible for initializing the CLUT for a 
*           particular display.
* Parms:    dptr - pointer at display block
* Returns:  XI_OK
* Date:     25 July 1991 (PORT FOM VMS)
* Author:   E. Scott Daniels
*
****************************************************************************
*/
#include "xiinclud.h"

Colormap syscolours = 0;                   /* the system colour map information */ 


#ifdef KEEP
char *colournames[ ] =            /* default colour table is small and based on x name strings */
{ 
	"black", "blue", "brown", "cyan", "gold", "darkgreen",
	"mediumblue", "grey", "limegreen", "orange", "pink", "firebrick",
	"tan", "darkslateblue", "white", "yellow", "purple",
	(char *) 0                   
};




//with the new colour management sceme, this is deprecated
int XIloadcolour2( struct display_blk *dptr )
{
 //long defblack;          /* default black colour */
 //long defwhite;          /* default white colour */
 int i;                  /* loop index */
 XColor hwcolour;         /* the closest rgb for the selected colour name */
 XColor rgb;             /* the rgb info for the colour */
 int status;             /* processing status information */
 //int r;
 //int g;
 //int b;                  /* temp colour values */
 XColor* dup_colour;

 //defwhite = WhitePixel( dptr->disp, dptr->screen );  /* get defaults */
 //defblack = BlackPixel( dptr->disp, dptr->screen );  /* get defaults */

 syscolours = DefaultColormap( dptr->disp, dptr->screen ); 			/* system supported colours */

 if( dptr->depth > 1 )               /* if a colour system */
  {
   for( i = 0; colournames[i] != NULL &&  i < (int) sizeof( colournames); i++ )   
    {
     status = XLookupColor( dptr->disp, syscolours, colournames[i], &rgb, &hwcolour );    /* get hardware approximation */
     if( status != XERROR )                /* look up was successful */
      {
       status = XAllocColor( dptr->disp, syscolours, &hwcolour ); 			/* translate name to pix value in syscolours */

		if( status != XERROR ) {   									/* if real colour allocated successfully */
			dup_colour = (XColor *) malloc( sizeof( XColor ) );
			memcpy( dup_colour, &hwcolour, sizeof( XColor ) );
			//dptr->clut[i] = hwcolour.pixel;      						/* save it in clut for our easy use */	
		}

        r = hwcolour.red;     /* save hw values for dark adjustment */
        g = hwcolour.green;
        b = hwcolour.blue;

         hwcolour.red |=  LIGHT_COLOUR_MASK;
         hwcolour.green |= LIGHT_COLOUR_MASK;
         hwcolour.blue |= LIGHT_COLOUR_MASK;

       status = XAllocColor( dptr->disp, syscolours, &hwcolour ); 			/* get colour info for the light flavour */

		if( status != XERROR )   										/* light colour allocated */
			dptr->clut[i+XI_LIGHT] = hwcolour.pixel;      					/* and  save it in clut */
		else
			dptr->clut[i+XI_LIGHT] = defwhite;            					/* default to white value on error */

        hwcolour.red = r & DARK_COLOUR_MASK;     			/* prep to allocate the dark version */
        hwcolour.green = g & DARK_COLOUR_MASK;
        hwcolour.blue = b & DARK_COLOUR_MASK;

       status = XAllocColor( dptr->disp, syscolours, &hwcolour ); 			/* get colour info and save in clut */

       if( status != XERROR )   /* light colour allocated */
         dptr->clut[i+XI_DARK] = hwcolour.pixel;      /* save it in clut */
       else
        dptr->clut[i+XI_DARK] = defwhite;            /* default to white value */

      }                                      
    }        
  }        

 else     								  /* monochrome system */
  {
   for( i = 0; colournames[i] != NULL  && i < (int) sizeof( colournames ); i++ )
    if( strcmp( colournames[i], "black" ) == 0 )  /* if black */
      dptr->clut[i] = defblack;                  /* make black black */
    else
      dptr->clut[i] = defwhite;                  /* make all others white */
  }         /* end mono system */

 return( XI_OK );          
}                         
#endif


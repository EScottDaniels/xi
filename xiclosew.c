/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XIclosew
* Abstract: This routine is responsible for closing a particular x window.
* Parms:    wnum - The number of the window (index into xiwins)
* Returns:  XI_OK if successful, XI_ERR_BADWNUM if bad window num passed
* Date:     25 July 1991 (PORT FOM VMS)
* Author:   E. Scott Daniels
*
* Modified: 30 Jul 1991 - To support unloading of the font
*           21 Aug 1991 - To support individual compiles 
****************************************************************************
*/
#include "xiinclud.h"      /* get our include stuff */

extern int XIclosew( wnum )
 int wnum;
{
 struct window_blk *wptr;    /* pointer to window block to close */
 struct button_blk *bptr;    /* pointer into button list to free */

 if( wnum < 0  || (wptr = XIvalidw( wnum )) == NULL )  /* invalid number passed in */ 
  return( XI_ERR_BADWNUM );

/*
with xft font we cant free like this; do we need to free at all?
 if( wptr->font )
  XFreeFont( dlist[wptr->dnum]->disp, wptr->font );
*/

//	XUnmapWindow( dlist[wptr->dnum]->disp, wptr->window );
 XDestroyWindow( dlist[wptr->dnum]->disp, wptr->window ); /* trash it */

 for( bptr = wptr->blist; bptr != NULL; )    /* free button blocks first */
  {
   if( bptr->next != NULL )
    {
     bptr = bptr->next;          /* get next one to free */
     free( bptr->prev );         /* and free the one we were just on */
    } 
   else                          /* last block */
    {
     free( bptr );
     bptr = NULL;                /* free it and exit loop */
    }
  }

 free( wptr );           /* drop our related storage */
 xiwins[wnum] = NULL;    /* indicate spot is available again */

 return( XI_OK );        /* to the bat poles Robin */
}                        /* XIclosew */

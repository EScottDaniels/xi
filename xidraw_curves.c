/*
=================================================================================================
	(c) Copyright 1995-2018 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
	Mnemonic:	XIdraw_curves
	Abstract:	These functions provide the ability to draw vrious curved 
				things: circles, elipses and arcs. Arcs and circles are 
				defined by a center point and radius while elipses are defined
				by a bounding rectangle anchored at the upper left corner.
	Date:		09 June 2018 
	Author:		E. Scott Daniels

****************************************************************************
*/
#include "xiinclud.h"

/*
	Draw an arc starting at angle 0 ending at angle 1. The arc is centered at 
	xo,yo and has the given radius.
*/
extern int XIdraw_arc( int wnum, int xo, int yo, int radius, int angle0, int angle1, int opts  ) {
	struct window_blk *wptr;					// windo information block
	Drawable target;

	if( (wptr = XIvalidw( wnum )) == NULL ) {
		return( XI_ERR_BADWNUM ); 					// bzzzzt, wrong
	}

	if( opts & XI_OPT_BB ) {				// select target based on options
		target = wptr->bbuffer;
	} else {
		target = wptr->window;
	}

	xo += wptr->orig_x;
	yo += wptr->orig_y;

	// X uses a rectangle, so we must convert center/radius into ull point and h/w
	XDrawArc( dlist[wptr->dnum]->disp, target, wptr->gc, xo-radius, yo-radius, radius*2, radius*2, angle0 * 64, angle1 * 64 );
	
	if( opts & XI_OPT_FLUSH ) {
		XFlush( dlist[wptr->dnum]->disp );
	}

	return( XI_OK );
}


/*
	Draw an elipse which fits in the rectangle with an upper left corner given by ulx, uly and 
	the ghiven height and width. 
*/
extern int XIdraw_elipse( int wnum, int ulx, int uly, int height, int width, int opts  ) {
	struct window_blk *wptr;					// windo information block
	Drawable target;

	if( (wptr = XIvalidw( wnum )) == NULL ) {
		return( XI_ERR_BADWNUM ); 					// bzzzzt, wrong
	}

	if( opts & XI_OPT_BB ) {				// select target based on options
		target = wptr->bbuffer;
	} else {
		target = wptr->window;
	}

	ulx += wptr->orig_x;
	uly += wptr->orig_y;

	if( opts & XI_OPT_FILL ) {
		XFillArc( dlist[wptr->dnum]->disp, target, wptr->gc, ulx, uly, width, height, 0, 360 * 64 );
	} else {
		XDrawArc( dlist[wptr->dnum]->disp, target, wptr->gc, ulx, uly, width, height, 0, 360 * 64 );
	}
	
	if( opts & XI_OPT_FLUSH ) {
		XFlush( dlist[wptr->dnum]->disp );
	}

	return( XI_OK );
}

/*
	Draw a circle given a center x,y and radius; just a specialised elipse
*/
extern int XIdraw_circle( int wnum, int xo, int yo, int radius, int opts  ) {
	return XIdraw_elipse( wnum, xo-radius, yo-radius, radius*2, radius*2, opts );
}

/*
	Given a center (xo,yo, and radius, draw a pie slice using the current colour.
	Angle0 and angle 1 are the start and endpoints of the arc. (0 degrees is 
	3 o'clock.) 
*/
extern int XIdraw_pie( int wnum, int xo, int yo, int radius, int angle0, int angle1, int opts ) {
	struct window_blk *wptr;					// windo information block
	Drawable target;

	if( (wptr = XIvalidw( wnum )) == NULL ) {
		return( XI_ERR_BADWNUM ); 					// bzzzzt, wrong
	}

	if( opts & XI_OPT_BB ) {				// select target based on options
		target = wptr->bbuffer;
	} else {
		target = wptr->window;
	}

	xo += wptr->orig_x;
	yo += wptr->orig_y;

	XSetArcMode( dlist[wptr->dnum]->disp, wptr->gc, ArcPieSlice );

	if( opts & XI_OPT_FILL ) {
		XFillArc( dlist[wptr->dnum]->disp, target, wptr->gc, xo-radius, yo-radius, radius*2, radius*2, angle0 * 64, angle1 * 64 );
	} else {
		XDrawArc( dlist[wptr->dnum]->disp, target, wptr->gc, xo-radius, yo-radius, radius*2, radius*2, angle0 * 64, angle1 * 64 );
	}

	if( opts & XI_OPT_FLUSH ) {
		XFlush( dlist[wptr->dnum]->disp );
	}

	return( XI_OK );
}

/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XIopenw
* Abstract: This routine is responsible for opening a window and gathering
*           all of the information that is to be kept in the window block.
* Parms:    pnum - Number of the parent (index into dlist if type is XI_PARENT
*                   otherwise index into xiwins if creating a child window)
*           type - Type of window Parent (child of display) | child
*           ulx  - X coordinate of the upper left corner
*           uly  - Y coordinate of upper left corner
*           width- Width in pixles of the window (if <0 defaults to parent's h/w)
*           height-Height in pixles of the window
*           fcolor-Color index (into the clut) of the initial foreground color
*           bcolor-Color index of the intial background color
*           name  - Pointer to the window name
* Returns:  The window handle (index into xiwin array) or XI_ERR error code
* Date:     25 July 1991 (PORT FOM VMS)
* Author:   E. Scott Daniels
*
* Modified: 30 Jul 1991 - To get a default font for the window
*            9 Sep 1991 - To set an event mask for testing 
*           13 Sep 1991 - To null the buttton list pointer.
*			01 Jul 2018 - Add double buffering support
****************************************************************************
*/
#include "xiinclud.h"
#ifdef KEEP
void XIwindow_szhints( int wnum, minh, minw, maxh, maxw  )
{
	XSizeHints hints;
 	struct window_blk *wptr;    /* pointer at window information block */

	memset( hints, 0, sizeof( hints ) );
	hints.min_width = minw;
	hints.min_height = minh;
	hints.max_width = maxw;
	hints.max_height = maxh;

	hints.flags = PMinSize | PMaxSize;
	if( (wptr = XIvalidw( wnum )) == NULL )
		return;

	XSetWMNormalHints( dlist[wptr->dnum]->disp, wptr->window. &hints );
}
#endif

int XIopenw( int pnum, int type, int ulx, int uly, int width, int height, int fcolor, int bcolor, char *name, int eventlist )
{
	int i;
	int fid;
	int wnum;                    		/* xi window number (index into xiwin array) */
	struct window_blk *wptr;     		/* pointer at the window informatino block */
	struct display_blk *dptr;    		/* pointer at the display block */
	unsigned long amask;         		/* attributes mask */
	unsigned int bordersize = 1; 		/* number of pixles in the border */
	XSetWindowAttributes attrs;  		/* attributes of the window */
	XSizeHints hints;            		/* hints information for window size */
	XWMHints wmhints;            		/* hints for the window manager */
	XGCValues gcvalues;          		/* graphic context values */
	Window parent;               		/* parent window information for new window */
	long mask;                   		/* create gc mask */
	long eventmask = ButtonPressMask | ExposureMask | KeyPressMask  | KeymapStateMask | FocusChangeMask | ButtonReleaseMask;

	if( eventlist )
		eventmask |= eventlist;		/* user supplied events */

	memset( (void *) &attrs, 0, sizeof( attrs ) );    		/* clear the structure */

	switch( type )
	{
		case XI_BORDERLESS_PARENT:	/* no frame, no title bar, no hot buttons, not moveable */
			bordersize = 0;
			// fallthrough	

		case XI_PARENT:
			if( pnum < 0 || pnum >= MAX_DISPLAY )
				return XI_ERR_BADDNUM;            		/* indicate bad info to caller */
	
			dptr = dlist[pnum];                   		/* point to display information */
			if( width < 0 )					/* -1 resorts to display size */
				width = dptr->width;
			if( height < 0 )
				height = dptr->height;

			parent = RootWindow( dptr->disp, dptr->screen );  		/* get parent to new */
			attrs.override_redirect = FALSE;    
			attrs.event_mask = (long) 0x00;  		/* ask for all now */    
			break;
	

		case XI_BORDERLESS_CHILD:
			bordersize = 0;
			// fallthrough	
			
		case XI_CHILD:
		default:
			if( (wptr = XIvalidw( pnum )) == NULL )  		/* is pnum a valid xiwin index ? */
				return( XI_ERR_BADPARENT );              		/* no - tell caller */
			dptr = dlist[wptr->dnum];    		/* get pointer to the display parent is on */
			parent = wptr->window;       		/* the the parent window info for X */

			if( width < 0 )
				width = wptr->width;
			if( height < 0 )
				height = wptr->width;

			attrs.override_redirect = FALSE; 
			break;
	}

	for( wnum = 0; wnum < MAX_WINDOWS && xiwins[wnum] != NULL; wnum++);		 /* find next avail window number */
	if( wnum >= MAX_WINDOWS )
		return XI_ERR_MAXOPEN; 

	xiwins[wnum] = wptr = (struct window_blk *) malloc(sizeof(struct window_blk));
	if( wptr == NULL )  
		return XI_ERR_NOMEM;    		

	if( height <= 0 ) /* || height >= dptr->height )*/
		height = dptr->height;                    		/* dont permit out of range */

	if( width <= 0 ) /*|| width >= dptr->width ) */
		width = dptr->width;                      		/* dont permit out of range */

	wptr->dnum = 0;                    		/* default display index */
		wptr->width = width;
	wptr->type = type;
	wptr->height = height;             		/* save the information in block */
	wptr->blist = NULL;                		/* no buttons right now */
	wptr->pressed = XI_ERROR;          		/* no spring button is pressed */
	wptr->orig_x = 0;
	wptr->orig_y = 0;
	wptr->scale = 1.0;
	for( i = 0; i < LASTEvent; i++ )   		/* initialize callback blocks */
	{
		wptr->cbs[i].cbdata = NULL;
		wptr->cbs[i].cbrtn = NULL;
	}

	attrs.background_pixel = dptr->clut[bcolor]->hwcolour.pixel;  		/* set up callers colors */
	attrs.border_pixel = dptr->clut[fcolor]->hwcolour.pixel;
	attrs.event_mask = eventmask;

	amask = CWBackPixel | CWBorderPixel | CWOverrideRedirect | CWEventMask;
	
	wptr->window = XCreateWindow( dptr->disp, parent, ulx, uly, width, height, bordersize, dptr->depth, InputOutput, CopyFromParent, amask, &attrs );
	
	if( wptr->window == 0 )    		/* error creating window ? */
	{
		free( wptr );                		/* clean up and return */
		xiwins[wnum] = NULL;         		/* this slot available again */
		return XI_ERR_WOPEN;    		/* let caller know of the difficulty here */
	}

	hints.flags = PPosition | PSize; 	/* set up to let wmanager know our ideas */
	hints.x = ulx;
	hints.y = uly;                    		/* indicate where we want it placed */
	hints.width = width; 
	hints.height = height;            		/* and how big we want it to be */
	hints.max_width = 0;
	hints.max_height =0;
	XSetWMNormalHints( dptr->disp, wptr->window, &hints );  		/* tell window mgr */
	
	wmhints.input = TRUE;   			/* have win manager set input focus and signal us */
	wmhints.initial_state = NormalState;     	/* open the window in plain sight */
	wmhints.flags = StateHint | InputHint;                  		/* set hints bit on */
	XSetWMHints( dptr->disp, wptr->window, &wmhints );      		/* send hints */

/*
	if( type == XI_BORDERLESS_PARENT )      				// if creating a parent (child to the display) 
		XIoff_borders( dptr->disp, wptr->window );
*/
	
	XMapRaised( dptr->disp, wptr->window);   		/* cause window to be painted */
	XFlush( dptr->disp );                    		/* cause command q to be flushed */
	
	if( type == XI_PARENT )                  		/* if creating a root window */
	{
		sleep( 1 );                		/* give the system a second to do its thing */
		XSelectInput( dptr->disp, wptr->window, (long) eventmask ); 
		/* XSetInputFocus( dptr->disp, wptr->window, 2, 0 ); */
		if( name != NULL )
			XStoreName( dptr->disp, wptr->window, name );
	}
	else                        		/* child stuff is different */
	{
		XSelectInput(dptr->disp, wptr->window, (long) eventmask ); 
	}


	/* create a graphics context to allow us to display stuff in windows */
	gcvalues.foreground = fcolor;
	gcvalues.background = bcolor;   		/* set user requested colors */
	
	gcvalues.function = GXcopy;					/* copy source in rather than xor and etc */
	mask = GCFunction | GCForeground | GCBackground;  		/* mask for gc create call */
	
	// swap action (last parm) is one of:
	// 	XdbeUndefined, XdbeBackground, XdbeUntouched, or XdbeCopied.
	wptr->bbuffer = XdbeAllocateBackBufferName( dptr->disp, wptr->window, XdbeBackground );

	wptr->gc = XCreateGC( dptr->disp, wptr->window, mask, &gcvalues );
	if( wptr->gc == XERROR )      
	{
		free( wptr );                		/* clean up and return */
		xiwins[wnum] = NULL;         		/* this slot available again */
		return( XI_ERR_WOPEN );    		/* let caller know of the difficulty here */
	}

	XSetForeground( dptr->disp, wptr->gc, fcolor ); 		/* set colors as passed */
	XSetBackground( dptr->disp, wptr->gc, bcolor ); 


	XFlush( dptr->disp );                    		/* cause command q to be flushed */

	//XIsetfont( wnum, "6x10" );    		/* load a default font - user can replace */
	fid = XIfind_font( wnum, "6x10" );				// find and load the font
	XIuse_font( wnum, fid );						// set it for use

	/*XIsetfont( wnum, "helvr10" );*/    /* load a default font - user can replace */
	
	return wnum;           		/* give the caller the info to access this window */
}                  

/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
***************************************************************************
*
*  Mnemonic: XIdebug
*  Abstract: Dump everything we  know at the moment about the window e
*            environment.
*  Parms:    None.
*  Date:     14 January 1997
*  Author:   E. Scott Daniels
*
***************************************************************************
*/
#include "xiinclud.h"

void XIdebug( )
{
 struct display_blk *dptr;       /* pointer at display block */
 struct window_blk *wptr;     /* pointer at window block */
 int i;                       /* loop index */

 for( i = 0; i < MAX_DISPLAY; i++ )
  {
   if( (dptr = dlist[i]) != NULL )
    {
     printf( "dnum[%d] = \n", i );
     printf( "\tdisp = %p\n", (void *) dptr->disp );
     printf( "\tscreen = %d\n", dptr->screen );
     printf( "\tw,h = %d, %d\n",dptr->width, dptr->height );
     printf( "\tdepth=%d\n", dptr->depth );
    }
   else
    printf( "dnum[%d] == NULL\n", i );
  }

for( i = 0; i < MAX_WINDOWS; i++ )
 {
  if( (wptr = xiwins[i]) != NULL )
   {
     printf( "xiwins[%d] = \n", i );
     printf( "\tbuttons = %p\n", (void *) wptr->blist );
   } 
 }

printf( "--------------------- %d\n", i );
}

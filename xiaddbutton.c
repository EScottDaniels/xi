/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XIaddbutton
* Abstract: This routine will add a button description block to the window's
*           button list. The button is optionally displayed (if the
*           XI_OPT_FLUSH option is specified (Even if XI_OPT_HIDE is set).
* Returns:  XI_WRN_DUPID is returned if a duplicate button id is located in
*           the list.
* Parms:    wnum    - window number (index into xiwins.)
*           x,y     - Coordinates of the button in the window
*           h,w     - Height and width of the button
*           cbpress - Address of routine called when button pressed
*           cbrel   - Address of routine called when button released
*           cbdata  - Address passed to callback routines when called
*           colour   - Color that the button is to be painted in
*           Text    - Pointer to string to display on the button
*           bid     - Id of the button for searches etc.  
*           type    - Type of button (radio, spring, toggle)
*           class   - Class that the button belongs to
*           option   - Background, initially hide/display button, flush (draw
*                   before leaving this routine)
* Date:     13 September 1991
* Author:   E. Scott Daniels
*	Mod:	20 Jun 2018 - Allow font for button.
*
****************************************************************************
*/
#include "xiinclud.h"

extern int XIaddbutton( int wnum, int x, int y, int h, int w, int (*cbpress)(), int (*cbrel)(), void *cbdata, int colour, int tcolour, char *text, int bid, int type, int class, int option )
{
 struct window_blk *wptr;    /* pointer at window information block */
 struct button_blk *bnew;    /* pointer at new block */
 struct button_blk *bptr;    /* pointer into the button list */
 //int doption;                /* option to pass to xidrawb */

 if( (wptr = XIvalidw( wnum )) == NULL )
  return( XI_ERR_BADWNUM );    /* let them know they blew it */

 bnew = (struct button_blk *) malloc( sizeof( struct button_blk ) );
 if( bnew == NULL )
  return( XI_ERR_NOMEM );   /* indicate failure */

 bnew->colour = colour;       /* initialize the structure */
 bnew->tcolour = tcolour; 
 bnew->x = x;
 bnew->y = y;
 bnew->height = h;
 bnew->width = w;
 bnew->bid = bid;
 bnew->type = type; 
 bnew->class = class;
 bnew->cbpress = cbpress;
 bnew->cbrel = cbrel; 
 bnew->data = cbdata;
 bnew->font = XIxft_find_font( wnum, "Helvetica", 10, XI_FS_BOLD );	// later we might allow it to be changed.
 bnew->flags = 0;             /* start with no flags set */

 bnew->text = (char *) malloc( (unsigned) strlen( text ) + 1 );
 if( bnew->text != NULL )
  strcpy( bnew->text, text );   /* save the text for the button */

 if( option & XI_OPT_BACKGROUND )   /* caller wants background for button */
  bnew->flags = bnew->flags | BF_BACK;  /* so turn this flag on */

 if( option & XI_OPT_HIDE )            /* dont display button yet */
  bnew->flags &=  ~BF_DISPLAY;
 else
  bnew->flags = bnew->flags | BF_DISPLAY;  /* indicate ok to paint it */

 bnew->next = wptr->blist;      /* add to the front of the list */
 bnew->prev = NULL;             /* initially no previous block */
 if( bnew->next != NULL )       /* if a next block exits */
  bnew->next->prev = bnew;      /* point it back to us */
 wptr->blist = bnew;            /* new head of the list */

 if( option & XI_OPT_IN )      /* if button is initially pressed in */
  bnew->flags |= BF_PRESSED;   /* then ensrue flag is set */

 if( option & XI_OPT_FLUSH )       /* caller wants display flushed */
  {
	XIxft_use_font( wnum, bnew->font );
   XIdrawb( wnum, x, y, h, w, colour, tcolour,  text, XI_OPT_FLUSH | option );
   bnew->flags = bnew->flags | BF_PAINTED;  /* indicated its on the screen */
  }

 for( bptr = bnew->next; bptr != NULL && bptr->bid != bid; bptr = bptr->next);

 if( bptr != NULL )           /* matching button id found (warning only) */
  return( XI_WRN_DUPID );     /* indicate duplicate id to caller */
 else
  return( XI_OK );
}                   /* XIaddbutton */

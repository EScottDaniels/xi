/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
----------------------------------------------------------------------------------
	Mnemonic:	xislider
	Abstract:	These functions implement a slider
			User invodkes XIslider and provides a pointer to data and
			a callback function that accepts (void *, int) where int
			is the current percentage (0-100).  For horizontal sliders
			(width > height) the 0 is when the button is in the left 
			most position. For vertical sliders, 0 is when the button
			is at the top.  The slider works a bit differently than 
			most with respect to when the pointer is placed off the 
			button and the left button is clicked. Instead of moving 
			the button down a 'buttons width' the button is jumpped to 
			the position of the pointer.  
	Date:		22 June 2008
	Author: 	E. Scott Daniels
----------------------------------------------------------------------------------
*/

#include "xiinclud.h"

#define XI_SLIDER_HORIZ		0x01
#define XI_SLIDER_INACT		0x02		/* user has disabled the slider */
#define XI_SLIDER_SHOWMID	0x04		/* show midpoint of window */

struct xi_slider {
	int	xiid;		/* window the slider is in */
	int	curp;		/* current percentage */
	int	height;
	int	width;
	int	lastp;		/* last x/y for motion */
	int	flags;
	int	bgcolour;	/* background colour */
	int	fgcolour;	/* background colour */
	int	bsize;		/* size of the button */
	int	threshold;	/* motion threshold */
	void	((*callback)( void *, int));	/* callback made each time pctg changes */	
	void	*callback_data;
};

/* ---------------------------------------------------------- */
static void xi_slider_draw( struct xi_slider *sp )
{
	if( sp == NULL ) {
		return;
	}

	if( sp->flags & XI_SLIDER_HORIZ )
	{
		XIdrawb( sp->xiid, sp->curp, 0, sp->height, sp->bsize, XI_GREY, XI_BLACK, "", 0 );	/* draw in new spot */

		if( sp->flags & XI_SLIDER_SHOWMID )
		{
			XIsetcolour( sp->xiid, sp->fgcolour );
			XIdrawl( sp->xiid, (sp->width/2)-(sp->bsize/2), 0, (sp->width/2)-(sp->bsize/2), sp->height, 0 );
			XIdrawl( sp->xiid, (sp->width/2)+(sp->bsize/2)-1, 0, (sp->width/2)+(sp->bsize/2)-1, sp->height, 0 );
		}
	}
	else
	{
		XIdrawb( sp->xiid, 0, sp->curp, sp->bsize, sp->width, XI_GREY, XI_BLACK, "", 0 );	/* draw in new spot */
		if( sp->flags & XI_SLIDER_SHOWMID )
		{
			XIsetcolour( sp->xiid, sp->fgcolour );
			XIdrawl( sp->xiid, 0, (sp->height/2)-(sp->bsize/2), sp->width, (sp->height/2)-(sp->bsize/2), 0 );
			XIdrawl( sp->xiid, 0, (sp->height/2)+(sp->bsize/2), sp->width, (sp->height/2)+(sp->bsize/2), 0 );
		}
	}
}

/* invoked when window is exposed; draw button */
static int xi_slider_expose( struct xi_slider *sp, int dnum, int win, XEvent *eptr )
{
	UNUSED( dnum );					// mark unused parms that we don't need to stop compiler warnings
	UNUSED( win );
	UNUSED( eptr );

	xi_slider_draw( sp );
	return XI_OK;
}


/* this is called for slider motion events */
int xi_slider_motion( struct xi_slider *sp, int dnum, int win, XEvent *eptr )
{
	int 	d;
	int	old_p;
	int	pp;			/* pointer position -- short for x or y */

	UNUSED( dnum );					// mark unused parms that we don't need to stop compiler warnings
	UNUSED( win );

	old_p = sp->curp;
	if( sp->flags & XI_SLIDER_HORIZ )
	{
		pp = eptr->xmotion.x;
		d = pp - sp->lastp;		/* movement delta */
		if( pp < 0 || pp > (sp->width) )
			return XI_OK;

		if( (sp->curp += d ) > sp->width-sp->bsize )
			sp->curp = sp->width-sp->bsize;
		else
			if( sp->curp < 0 )
				sp->curp = 0;

		sp->lastp = pp;
		XIsetcolour( sp->xiid, sp->bgcolour );
		XIdrawr( sp->xiid, old_p, 0, sp->height, sp->bsize, XI_OPT_FILL );			/* erase present button */ 
		xi_slider_draw( sp );
	}
	else
	{
		pp = eptr->xmotion.y;
		d = pp - sp->lastp;
		if( pp < 0 || pp > sp->height )
			return XI_OK;

		if( (sp->curp += d) > sp->height-sp->bsize )
			sp->curp = sp->height-sp->bsize;
		else
			if( sp->curp < 0 )
				sp->curp = 0;

		sp->lastp = pp;
		XIsetcolour( sp->xiid, sp->bgcolour );
		XIdrawr( sp->xiid, 0, old_p, sp->bsize, sp->width, XI_OPT_FILL );			/* erase present button */ 
		xi_slider_draw( sp );

	}

	if( sp->callback != NULL ) {
		if( sp->flags & XI_SLIDER_HORIZ ) {
			(sp->callback)( sp->callback_data,  (100*sp->curp)/(sp->width-sp->bsize) );
		} else {
			(sp->callback)( sp->callback_data,  100 - (100*sp->curp)/(sp->height-sp->bsize) );
		}
	}

	return 0;
}

int xi_slider_button_release( struct xi_slider *sp, int dnum, int win, XEvent *eptr )
{
	UNUSED( dnum );					// mark unused parms that we don't need to stop compiler warnings
	UNUSED( win );
	UNUSED( eptr );
	UNUSED( sp );

	/* nothing to do here at this point */
	return 0;
}

int xi_slider_button_press( struct xi_slider *sp, int dnum, int win, XEvent *eptr )
{
	int pct;

	UNUSED( dnum );					// mark unused parms that we don't need to stop compiler warnings
	UNUSED( win );

	if( sp->flags & XI_SLIDER_HORIZ )
	{
		sp->lastp = eptr->xbutton.x;

		if( eptr->xbutton.x >= sp->curp && eptr->xbutton.x <= sp->curp+sp->bsize )	/* click on button */
			return 0;

		XIsetcolour( sp->xiid, sp->bgcolour );
		XIdrawr( sp->xiid, sp->curp, 0, sp->height, sp->bsize, XI_OPT_FILL );			/* erase present button */ 

		sp->curp = eptr->xbutton.x > (sp->width - sp->bsize) ? sp->width-sp->bsize : eptr->xbutton.x;
		xi_slider_draw( sp );

		if( sp->callback != NULL )
		{
			pct = (100*sp->curp)/(sp->width-sp->bsize);
			(sp->callback)( sp->callback_data,  pct > 100 ? 100 : pct );
		}
	}
	else
	{
		sp->lastp = eptr->xbutton.y;
		if( eptr->xbutton.y >= sp->curp && eptr->xbutton.y <= sp->curp+sp->bsize )	/* click on button */
			return 0;

		XIsetcolour( sp->xiid, sp->bgcolour );
		XIdrawr( sp->xiid, 0, sp->curp, sp->bsize, sp->width, XI_OPT_FILL );			/* erase present button */ 
		sp->curp = eptr->xbutton.y > (sp->height - sp->bsize) ? sp->height-sp->bsize : eptr->xbutton.y;
		xi_slider_draw( sp );

		if( sp->callback != NULL )
		{
			pct = (100*sp->curp)/(sp->width-sp->bsize);
			(sp->callback)( sp->callback_data,  pct > 100 ? 100 : pct );
		}

		if( sp->callback != NULL )
		{
			pct = (100*sp->curp)/(sp->height-sp->bsize);
			(sp->callback)( sp->callback_data,  pct > 100 ? 100 : pct );
		}
	}

	return 0;
}


struct xi_slider *XIslider( int pnum, int ulx, int uly, int width, int height, int bsize, int fcolour, int bcolour, int initv, void *cb_data, void ((*callback)( void *, int )) )
{
	struct xi_slider *sp;

	if( (sp = (struct xi_slider *) malloc( sizeof( struct xi_slider ) )) == NULL ) {
		return NULL;
	}

	sp->height = height;
	sp->width = width;
	sp->bgcolour = bcolour;
	sp->fgcolour = fcolour;
	sp->bsize = bsize;
	sp->callback = callback;
	sp->callback_data = cb_data;
	sp->flags |= XI_SLIDER_SHOWMID;

	if( initv > 0 )
	{
		if( initv <= 100 )
			sp->curp = initv;
		else
			sp->curp = 100;
	}

	if( (sp->threshold = width/100) < 1 )
		sp->threshold = 1;

	sp->xiid = XIopenw( pnum, XI_CHILD, ulx, uly, width, height, fcolour, bcolour, "slider",  XI_MOTIONB1 | XI_VISCHANGE );
	XIcbreg( sp->xiid, MotionNotify, &xi_slider_motion, sp );
	XIcbreg( sp->xiid, ButtonRelease, &xi_slider_button_release, sp );
	XIcbreg( sp->xiid, ButtonPress, &xi_slider_button_press, sp );
	XIcbreg( sp->xiid, Expose, &xi_slider_expose, sp );

	if( width > height )
	{
		sp->flags |= XI_SLIDER_HORIZ;
		sp->curp = ((sp->width-sp->bsize) * sp->curp)/100;		/* convert pct to actual x value */
		xi_slider_draw( sp );
	}
	else
	{
		sp->curp = ((sp->height-sp->bsize) * sp->curp)/100;		/* convert pct to actual x value */
		xi_slider_draw( sp );
	}

	return sp;
}



void XIclose_slider( struct xi_slider *sp )
{
	if( sp == NULL )
		return;

	XIcbunreg( sp->xiid, MotionNotify );
	XIcbunreg( sp->xiid, ButtonRelease );
	XIcbunreg( sp->xiid, ButtonPress );
	XIcbunreg( sp->xiid, Expose );
	XIclosew( sp->xiid );
	free( sp );	
}

/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XIclosed
* Abstract:  This routine is responsible for closing all open displays.
*            The routine will also close any associated windows.
* Parms:    None.
* Returns:  Nothing.
* Date:     25 July 1991 (PORT FOM VMS)
* Author:   E. Scott Daniels
*
* Modified: 21 Aug 1991 - To make individually compilable
****************************************************************************
*/
#include "xiinclud.h"    /* get our include info as an external routine */

extern void XIclosed( )
{
 //struct display_blk *dptr;   /* pointer to display blocks */
 int i;                      /* loop index */

 for( i = MAX_WINDOWS-1; i >= 0; i-- )   /* close all open windows */
  XIclosew( i );                  /* do it in reverse to close children 1st */

 for( i = 0; i < MAX_DISPLAY; i++ )  /* close the displays that are open */
  {
   if( dlist[i] != NULL )            /* is this display open? */
    {
     XCloseDisplay( dlist[i]->disp );  /* call x to close it */
     free( dlist[i] );                 /* drop allocated storage */ 
    }
  }
}     /* XIclosed */

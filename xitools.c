
/*
	Mnemonic:	xitools.c
	Abstract:	Private tools.
	Date:		10 June 1028
	Author:		E. Scott Daniels
*/

#include <stdlib.h>
#include <string.h>

/*
	Malloc_dup accepts a pointer to a zero terminated string, allocates a new
	buffer, and the copies the original string to the newly allocated buffer.
	Return value is the pointer, or nil on error.

	Strdup is not a part of any Cxx standard so we must implement our own.
*/
extern char* malloc_dup( char const* src ) {
	char*	target;
	int		n;				// size of stirng

	n = strlen( src ) + 1;			// account for nil termination char
	target = (char *) malloc( sizeof( char ) * n ); 
	if( target ) {
		memcpy( target, src, n );
	}

	return target;
}


#ifdef SELF_TEST
#include <stdio.h>

int main( ) {
	char const* s = "foo bar";
	char* dstr;

	dstr = malloc_dup( s );
	if( strcmp( dstr, s ) != 0 ) {
		fprintf( stderr, "[FAIL] duplicates string does not match source: `%s` != `%s`\n", dstr, s );
		return 1;
	}

	fprintf( stderr, "[PASS] strings match: `%s` == `%s`\n", dstr, s );
	return 0;

}
#endif

/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
*
*  Mnemonic:  XIdriver (xidriver.c)
*  Abstract:  This routine will poll each display for events and drive 
*             the proper callback routine when an event is received.
*             When called in normal mode, the routine will "block" and 
*             return to the caller only when the status XI_RET_QUIT is 
*             returned by a callback. In poll mode, the routine will 
*             return to the caller when no more X events are pending.
*  Parms:     mode  - operation mode (XI_MODE_NORM or XI_MODE_POLL)
*  Returns:   XI_OK if processing should continue (poll mode)
*             XI_RET_QUIT if callback forced stop of routine
*  Date:      9 January 1997
*  Author:    E. Scott Daniels
*  Mod:		08 Jun 2009 - added polling with checkmaskevent
*
***************************************************************************
*/
#include "xiinclud.h" 

int XIdriver( int mode ) {
	int dnum;          /* index into display list */
	int status = 0;    /* status from last callback */
	int ((*cbrtn)());  /* pointer to routine to call */
	Window ewindow;    /* X window handle of the event window */
	int wnum;          /* our window number - index into window list */
	XEvent event;      /* event that has occured in a window */
	XAnyEvent *eptr;   /* use to get specific info avalil nomatter what event */

	eptr = (XAnyEvent *) &event;     /* "convert" to get more info */

	do {													/* main loop - until quit or no events */
		for( dnum = 0; dnum < MAX_DISPLAY; dnum++ ) {		/* for each open display */
			if( dlist[dnum] != NULL ) {						/* ensure its open */
				if( mode == XI_MODE_NORMAL ) {
					XNextEvent( dlist[dnum]->disp,(XEvent *) &event );  /* get the event */
				} else {
					if( ! XCheckMaskEvent(  dlist[dnum]->disp, XI_POLL_EVENTS, (XEvent *) &event ) ) {
						return XI_OK;
					}
				}

				ewindow = eptr->window;

				for( wnum = 0; wnum < MAX_WINDOWS && (xiwins[wnum] == NULL || xiwins[wnum]->window != ewindow); wnum++ );
				

				if( wnum < MAX_WINDOWS ) {         /* found the window in our list */
					if( event.type == ConfigureNotify ) {
						while( XCheckTypedWindowEvent( dlist[dnum]->disp, xiwins[wnum]->window, ConfigureNotify, (XEvent *) eptr ) );
					}
	
					if( (cbrtn = xiwins[wnum]->cbs[event.type].cbrtn) != NULL ) {
						status = (*cbrtn)( xiwins[wnum]->cbs[event.type].cbdata, dnum, wnum, (XEvent *) &event );
					}
				}
			} 
		}   
	} while( mode == XI_MODE_NORMAL && status != XI_RET_QUIT );

	return status;			          /* let em know why we left */
}                                                         /* XIdriver */

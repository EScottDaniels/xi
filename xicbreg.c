/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
*
*  Mnemonic: XIcbreg
*  Abstract: This routine will register a callback in the table for a 
*            designated window. When an event is received on the window
*            the associated callback routine is invoked. If no callback 
*            is registered the event is ignored. 
*  Parms:    wnum - The XI window number to which the cb is registered. 
*            type - Type of callback to register (we use x event names
*                   listed in X.h to allow any x event and to speed the 
*                   look up of the cb routine.
*            fptr - Pointer to the function to register 
*            data - Pointer that the user wants the callback function to get 
*  Returns:   XI_OK if we found the window, XI_ERR_BADWNUM if error 
*  Date:     23 January 1995
*  Author:   E. Scott Daniels
*
****************************************************************************
*/
#include "xiinclud.h"     /* get defs and stuff */

/*
	Register an event callback.
*/
extern int XIecbreg( int wnum, int type, void ((*fptr)( struct edit_blk*, int, int, XEvent* )), void * data ) {
 struct window_blk *wptr;      			/* pointer at window block of information */
 int status = XI_ERR_BADWNUM ; 			/* status */

 if( (wptr= XIvalidw( wnum )) != NULL )
  {
   if( type > 1  && type < LASTEvent )  /* ensure they are in range */
    {
     wptr->cbs[type].cbdata = data;      /* save pointer to data */
     wptr->cbs[type].ecbrtn = fptr;      /* save pointer to function */
     status = XI_OK;                     /* good return status */
    }
   else
    status = XI_ERR_BADEVENT;            /* let caller know problem */
  }

 return( status ); 
}

extern int XIcbreg( int wnum, int type, int ((*fptr)()), void * data )
{
 struct window_blk *wptr;      /* pointer at window block of information */
 int status = XI_ERR_BADWNUM ; /* status */

 if( (wptr= XIvalidw( wnum )) != NULL )
  {
   if( type > 1  && type < LASTEvent )  /* ensure they are in range */
    {
     wptr->cbs[type].cbdata = data;      /* save pointer to data */
     wptr->cbs[type].cbrtn = fptr;       /* save pointer to function */
     status = XI_OK;                     /* good return status */
    }
   else
    status = XI_ERR_BADEVENT;            /* let caller know problem */
  }

 return( status ); 
}                                      /* XIcbreg */

void XIcbunreg( int wnum, int type )
{
	struct window_blk *wptr;

	if( (wptr= XIvalidw( wnum )) != NULL )
	{
		if( type > 1  && type < LASTEvent )  /* ensure they are in range */
		{
			wptr->cbs[type].cbdata = NULL; 
			wptr->cbs[type].cbrtn = NULL; 
		}
	}
}

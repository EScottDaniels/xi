/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XImodbutton
* Abstract: This routine modifies a "button" on the list for a particular
*           window. The following option can be used to modify the button.
*           If the option is set then the cooresponding value in the
*           button block is changed to the value passed in. Other parameters
*           are used only when the coresponding option is set.
*             XI_MOD_DEL  - Delete the button.
*             XI_MOD_BID  - Change the button id
*             XI_MOD_XY   - Change the xy coordinates of the button
*             XI_MOD_HW   - Modify the height and width
*             XI_MOD_REL  - Change the xy, hw information relative to the
*                           current values (add the new values to current)
*             XI_MOD_COLOR- Change the colour value of the button
*             XI_MOD_HIDE - Hide the button (dont allow it to be painted)
*             XI_MOD_SHOW - Show the button (allow it to be painted)
*             XI_MOD_CALL - Change the address of the callback routine
*             XI_MOD_TEXT - Change the text string
*             XI_MOD_CLASS- Change the class of the button
*             XI_MOD_PRESS- Cause button to be displayed pressed in 
*             XI_MOD_RELEASE - Cause to be displayed popped out 
* Returns:  XI_ERR_BADID - Button id not in the list.
*           XI_OK        - Just fine!
* Parms:    wnum - window number (index into xiwins.)
*           x,y  - Coordinates of the button in the window
*           h,w  - Height and width of the button
*           prescb - New address of the routine to callback when pressed
*           relcb  - New address of routine to call when button is released
*           colour- Color that the button is to be painted in
*           Text - Pointer to string to display on the button
*           bid  - Id of the button (returned when button pressed if no callb
*           class- Class that the button belongs to
*           option- Background, initially hide/display button, flush (draw
*                   before leaving this routine)
* Date:     13 September 1991
* Author:   E. Scott Daniels
*
****************************************************************************
*/
#include "xiinclud.h"



//int XImodbutton( int wnum, int x, int y, int h, int w, void *prescb, void *relcb, int colour, int tcolour, char *text, int bid, int class, int option )
int XImodbutton( int wnum, int x, int y, int h, int w, int (*prescb)(void), int (*relcb)(void), int colour, int tcolour, char *text, int bid, int class, int option )
{
 struct window_blk *wptr;    /* pointer at window information block */
 struct button_blk *bptr;    /* pointer into the button list */
 //int doption;                /* option to pass to xidrawb */
 //int oldcolour;               /* prevous fg colour of the window */

 if( (wptr = XIvalidw( wnum )) == NULL )
  return( XI_ERR_BADWNUM );    /* let them know they blew it */

	for(bptr = wptr->blist; bptr != NULL && bptr->bid != bid; bptr = bptr->next );
	if( bptr == NULL ) {
		return( XI_ERR_BADID );
	}


 if( option & XI_MOD_DEL )             /* delete the block ? */
  {
   if( bptr->prev != NULL )             /* something on top? */
    	bptr->prev->next = bptr->next;
   if( bptr->next != NULL )
    bptr->next->prev = bptr->prev;
   if( wptr->blist == bptr )             /* if loosing the top one */
    	wptr->blist = bptr->next;           /* send it down one */
   XIdrawr( wnum, bptr->x, bptr->y, bptr->height, bptr->width,
            XI_OPT_FLUSH | XI_OPT_FILL );
   free( bptr );                           /* loose the block */
  }
 else                                  /* check for any other option */
  {
   	if( option & XI_MOD_BID )           /* change the button id */
    	bptr->bid = bid;

   	if( option & XI_MOD_XY )   /* reset the x and y coords of the button */
	{
    	if( option & XI_MOD_REL )  /* set the relative to the current settings */
     	{
      		bptr->x += x;
      		bptr->y += y;    /* add the values passed in */
     	}
    	else               /* replace with values passed in */
     	{
    	  	bptr->x = x;
      		bptr->y = y;
     	}
	}

   if( option & XI_MOD_HW )   /* mod the height and width of the button */
	{
    	if( option & XI_MOD_REL )  /* set them relative to the current settings */
     	{
      		bptr->height += h;
      		bptr->width += w;    /* add the values passed in */
     	}
    	else               /* replace with values passed in */
     	{
      		bptr->height = h;
      		bptr->width = w;
     	}
	}

	if( option & XI_MOD_COLOR )  /* change the colour */
	{
     		bptr->colour = colour;
     		bptr->tcolour = tcolour;
	}

    if( option & XI_MOD_HIDE )  /* hide the button */
     {
      XIsetcolour( wnum, bptr->colour );
      bptr->flags &= ~BF_DISPLAY;                   /*turn off display flg*/
      bptr->flags &= ~BF_PAINTED;                   /*turn off painted flg*/
      XIdrawr( wnum, bptr->x, bptr->y, bptr->height, bptr->width,
               XI_OPT_FLUSH | XI_OPT_FILL );
     }

    if( option & XI_MOD_LOCK )   /* set the lock flag ? */
      bptr->flags |= BF_LOCKED;

    if( option & XI_MOD_UNLOCK )
      bptr->flags &= ~BF_LOCKED;   /* turn off lock */

    if( option & XI_MOD_SHOW )             /* now we can display the button */
      bptr->flags = bptr->flags | BF_DISPLAY;  /* indicate ok to paint */

    if( option & XI_MOD_CALL )   /* update callback information */
     {
      bptr->cbpress =  prescb;    
      bptr->cbrel =  relcb;
     }

    if( option & XI_MOD_CLASS )  /* update the class information */
     bptr->class = class;

    if( option & XI_MOD_TEXT )   /* new text string */
     {
      if( bptr->text != NULL )
       free( bptr->text );        /* loose the string if there */

      bptr->text = (char *) malloc( (unsigned) strlen( text ) + 1 );
      if( bptr->text != NULL )
       strcpy( bptr->text, text );   /* save the text for the button */
     }

    if( option & XI_MOD_PRESS )      /* cause button to be displayed pressed */
     bptr->flags |= BF_PRESSED;

    if( option & XI_MOD_RELEASE )    /* cause button to show poped out */
     bptr->flags &= ~BF_PRESSED;

    if( bptr->flags & BF_DISPLAY )         /* if ok to display the button */
     XIdispbutton( wnum, bptr->bid, XI_OPT_FLUSH | XI_OPT_ONE );
   }     /* end else - button not deleted  */

 return( XI_OK );
}                   /* XIdispbutton */


/*
	Delete all buttons with the given class.
*/
extern void XIdel_buttons( int wnum, int class ) {
	struct window_blk *wptr;    /* pointer at window information block */
	struct button_blk *bptr;    /* pointer into the button list */
	struct button_blk *next;    // next button in loop; allow delete

	if( (wptr = XIvalidw( wnum )) == NULL ) {
		return;
	}

 	for( bptr = wptr->blist; bptr != NULL; bptr = next ) {
		next = bptr->next;

		if( bptr->class == class ) {
			if( bptr->prev != NULL ) {             		// remove from the list
				bptr->prev->next = bptr->next;
			}
			if( bptr->next != NULL ) {
				bptr->next->prev = bptr->prev;
			}
			if( wptr->blist == bptr ) {
				wptr->blist = bptr->next;
			}

   			free( bptr );
  		}
	}
}

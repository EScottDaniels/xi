/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
*
*  Mnemonic: xiproto.h
*  Abstract: This file contains the prototypes for the XI___ routines.
*            it should be included by any process that will link in the 
*            libxi.a library.
*
****************************************************************************
*/
#ifndef __xiproto
#define __xiproto

// ---------- composite things (buttons, sliders, prompts) ----------------------------------------------
int XIaddbutton( int wnum, int x, int y, int h, int w, int (*cbpress)(), int (*cbrel)(), void *cbdata, int colour, int tcolour, char *text, int bid, int type, int class, int option);
int XIbtnmgr( int wnum, XButtonEvent *event );
//int XImodbutton( int wnum, int x, int y, int h, int w, void *prescb, void *relcb, int colour, int tcolour, char *text, int bid, int class, int option );
extern int XImodbutton( int wnum, int x, int y, int h, int w, int (*prescb)(void), int (*relcb)(void), int colour, int tcolour, char *text, int bid, int class, int option );
extern void XIdel_buttons( int wnum, int class );
extern XImage *XInewimage( int window, int height, int width,  int depth, void *pxmap, int bpp, double scale );
#ifdef KEEP
extern void XIoff_borders( Display *d, int w );
#endif
extern int XIpmgr( struct prompt_blk *pptr, int dnum, int wnum, XEvent *eptr );
extern int XIprompt( int parent, char *text, char *buf, int blen, struct bdef_blk *blist, int ((*cbrtn)()), void *cbdata );
extern struct xi_slider *XIslider( int pnum, int ulx, int uly, int width, int height, int bsize, int fcolour, int bcolour, int maxv, void *cb_data, void ((*callback)( void *, int )) );
extern XImage *XIupdateimage( int window, XImage *image, int height, int width,  int udepth, unsigned char *pxmap, int bpp, double scale );


// --------- display window and callback -------------------------------------------------------
extern int XIclearw( int window );
extern void XIclosed( );
extern int XIclosew( int wnum );

extern int XIcbreg( int wnum, int type, int ((*fptr)()), void * data );
extern int XIecbreg( int wnum, int type, void ((*fptr)( struct edit_blk*, int, int, XEvent*)), void * data );
extern void XIcbunreg( int wnum, int etype );

extern int XImap( int wnum );
extern int XImovew( int wnum, int height, int width );
extern void XIraise( int wnum );
extern int XIresizew( int wnum, int height, int width );
extern void XIlower( int wnum );

extern int XIopend( char *dname );
extern int XIopenw( int pnum, int type, int ulx, int uly, int width, int height, int fcolour, int bcolour, char *name, int eventmask );
extern struct window_blk *XIvalidw( int wnum );
extern int XIunmap( int wnum );

// ----------  graphics context ------------------------------------------------------
extern int XIline_attrs( int window, int dashed, int width, int cap_style, int join_style );

// ----------- colour and clut -------------------------------------------------------
extern int XIdefcolour( int wnum, int clutnum, int r, int g, int b );
extern int XIloadcolour( struct display_blk *dptr );
extern int XIsetcolour( int wnum, int colour );
extern int XIset_default_colours( struct display_blk *d );


// ----------- font and xft font functions ------------------------------------------
extern int XIfind_font( int wnum, char* font_name );
extern int XIuse_font( int wnum, int font_id );
extern void XIdump_font_info( int wnum, char* pattern );
extern int XIsetfont( int wnum, char *fname );

extern int XIxft_find_font( int wnum, char* font_family, int size, int style );
extern int XIxft_use_font( int wnum, int font_id );
extern int XIxft_get_smetrics( int wnum, int font_id, char* string, int* height, int* width );

// ------------ double buffering --------------------------------------------------
extern void XIswap_buffers( int wnum );

// ------------ drawing functions ------------------------------------------------
extern void XIdrawimage( int window, XImage *image, int xo, int yo, int startx, int starty );
extern int XIdispbutton( int wnum, int bid, int option );
extern int XIdrawb( int wnum, int ulx, int uly, int height, int width, int colour, int tcolour, char *text, int option );
extern int XIdrawl( int window, int x1, int y1, int x2, int y2, int option );
extern int XIdrawp( int window, XPoint *points, int count, int option );
extern int XIdrawr( int wnum, int ulx, int uly, int height, int width, int option );
extern int XIdraw_arc( int wnum, int xo, int yo, int radius, int angle0, int angle1, int opts  );
extern int XIdraw_elipse( int wnum, int ulx, int uly, int height, int width, int opts  );
extern int XIdraw_circle( int wnum, int xo, int yo, int radius, int opts  );
extern int XIdraw_pie( int wnum, int xo, int yo, int radius, int angle0, int angle1, int opts );

extern int XIdrawtext( int window, int x, int y, char *str );
extern int XIdrawstring( int window, int x, int y, char *str );		/* uses drawImageString rather than DrawString */
extern int XIimdrawtext( int window, XImage *image, int x, int y, char *str );
extern int XIxft_draw_text( int wnum, int xo, int yo, int colour_id, int font_id, char* string, int opts );

extern int XIdrawp_voidp( int window, void* points, int count, int option );
extern void* XIpt_alloc( int n );
extern void XIpt_add( void* vpts, int n, int x, int y );
extern void XIpt_free( void* vpts );
extern void* XIpt_xlate( struct window_blk* wptr, XPoint* src_pts, int n );

// ------------ back buffer drawing functions ------------------------------------

extern int XIflush( int window );

// ---------------- event and run time -----------------------------------------------------
int XIdriver( int mode );

void XIemgr( struct edit_blk *eptr, int dnum, int wnum, XEvent *event );
extern int XIevent( int parent, XEvent *event );
extern int XIpoll( int parent, XEvent *event, int *ewindow );
extern int XIfindbutton( int wnum, int x, int y );
extern int XIfocus( int window );

// -------------- memory management ---------------------------------------------------------
extern void XIfreeimage( XImage *image );
extern int XIget_colour_rgb( int wnum, int colour_id, unsigned short* red, unsigned short* green, unsigned short* blue );
extern int XIinit( );
extern char* malloc_dup( char const* src );
extern int XImakecb( int dnum, int wnum, XEvent *event );

// --- misc things ------------------------------------------------------------------------ ---
/*int XImovew( int wnum, int x, int y );*/
extern void XIscale( int wnum, double new_scale );
extern void XIdelta_xlate( int wnum, int delta_x, int delta_y );
extern void XIxlate( int wnum, int x, int y );



extern void XIset_err_display( int silent );		/* false writes xerrs to stderr; true keeps them quiet */
extern int XIsetolay( int wnum, int fun );
extern int XIstrlen( int wnum, char *str );

#endif

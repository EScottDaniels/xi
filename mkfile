# mk not make!
#
#verbose= -pedantic -std=c99 -Wall -Wextra -Wshadow -Werror
verbose= -pedantic -std=c11 -Wall -Wextra -Wshadow -Werror

libs = -L../lib -lut 
png_libs = -L/usr/local/lib -lpng
sys_libs = /usr/X11R6/lib/libX11.so -lm 

CFLAGS = -g -I /usr/local/include -I /usr/include -I /usr/include/freetype2 -I ../lib -I /usr/X11R6/include $verbose
xcc = gcc   $CFLAGS  -o $target $libs
ar = ar -cr libxi.a 
libs = -L../lib -lut /usr/X11R6/lib/libX11.so -lm 

%.o:: %.c xi.h xiproto.h xistruct.h 
	gcc   $CFLAGS  -c ${prereq%% *} -o $target 

headers = xiconst.h xiinclud.h xistruct.h xitypes.h xivars.h

#
# lib source
#
#xidraw_curves
src = xiclosed xiclosew xidrawl xidrawr xidrawimage xidrawp xiinit \
 xiopend xiopenw xisetcolour xisetolay xivalidw xidrawtext\
 xievent xidefcolour xiaddbutton xidispbu xidrawb xifocus xifindbutton\
 ximodbutton xibtnmgr xipoll xicbreg xidriver xidebug xipmgr\
 xiprompt xiemgr xistrlen xifonts xiclearw xislider xioff_borders ximap ximovew xiraise xiresizew\
 xipmap xitools xixft xiflush xidbe xiline_attrs xixlate

libxi.a:: xicolours.h ${src:=%.o} 
	ar cr libxi.a $prereq

xicolours.h:: xidefcolour.c
	gen_xi_colours.ksh >$target

nuke::
	rm -f *.o *.a

tool_test: xitools.c
	gcc $CFLAGS -DSELF_TEST xitools.c -o tool_test 

test1 : libxi.a test1.c
	gcc $CFLAGS  test1.c -o $target  -L. -lxi  $libs $png_libs $sys_libs

test12::
	gcc $CFLAGS -I . -I ../lib  test12.c  -o test12 -L.  -lxi $libs

verify::
	echo cflags = $CFLAGS
	echo $png_libs
	echo $libs

backup::
	tar -cf - mkfile *.c *.h | gzip >xi.back.tgz
	scp xi.back.tgz slug:

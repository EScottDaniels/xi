/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
*
*  Mnemonic: XIemgr (xiemgr.c)
*  Abstract: This routine is driven when events are received on an edit
*            window. The routine is passed the keyevent and the data 
*            pointer should point to a valid edit block. The event is 
*            handeled and contents of the window are updated. This is a
*            simple edit manager that will handle editing one line in a 
*            window - to support prompt windows. If an application needs
*            multi line editing, the application will need to supply its 
*            own edit manager that is registered to the window.
*  Parms:    eptr  - Pointer to the edit block (data)
*            dnum  - Display on which the event occured
*            wnum  - Window on which the event occred 
*            event - Pointer to the event block describing the event
*  Returns:  XI_OK if allok, XI_RET_ERROR if we have a severe problem here.
*  Date:     27 January 1997
*  Author:   E. Scott Daniels
*
*****************************************************************************
*/
#include "xiinclud.h"
#include <ctype.h>

void XIemgr( struct edit_blk *eptr, int dnum, int wnum, XEvent *event )
{
 XKeyEvent *kevent;    /* pointer to key event block */
 int cursorx;          /* x pix location for cursor */
 char buf[11];         /* character conversion buffer */
 int flags = 0;        /* local flags */

	if( dnum < 0 || wnum < 0 )
		return;

 switch( event->type )
  {
   case KeyPress:                  /* keyboard event? */
     kevent = (XKeyEvent *) event;   /* conversion of pointer type */

     if( XLookupString( kevent, buf, 10,  NULL, NULL ) > 0 ) 
      {
       if( isprint( *buf ) )     /* printable - just add to buffer and show */
        {
         if( eptr->idx < eptr->blen-1 )     /* still room in the buffer? */
          {
           eptr->buf[eptr->idx++] = *buf;
           eptr->buf[eptr->idx] = EOS;         /* ensure nice term */
           flags |= LF_DISPLAY;                  /* display string later */
          }                                 /* end if room in the buffer */ 
        }                                  /* end if printable character */
       else
        switch( *buf )                      /* handle "command" character */
         {
          case 0x0d:             /* return key */
            break;
    
          case 0x08:                          /* backspace key or ctl-h */
            if( eptr->idx > 0 )
             {
              flags |= LF_DISPLAY + LF_BLANK;   /* back up one and display */
              eptr->idx--;
              eptr->buf[eptr->idx] = EOS;      /* blank what was there */
             }
            break;
    
          case 0x0c:             /* ctl-l - forward space */
            break;
    
          case 0x1b:             /* escape key */
            break;
    
          default:
            break;
         }                       /* end switch on command character */
      }                   /* end if character from key conversion successful */
    break;                          /* end key press event type */

   case ButtonPress:
   case FocusIn:                /* focus is now on this window */
    flags |= LF_DISPLAY;        /* cause repaint - with cursor */ 
    XIfocus( eptr->window );    /* grab focus so pointer can move outside */
    break;

   case FocusOut:                      /* focus not on this window */
    flags |= LF_NOCURSOR;              /* redisplay without cursor */
    break;

   case Expose:
    XIdispbutton( eptr->window, 0, XI_OPT_ALL );  /* display our button area */
    flags |= LF_DISPLAY;
    break;

   default:
    break;
  }                            /* end switch on event type */
    
                          /* event handled - redisplay text/cursors??? */
 if( flags & LF_DISPLAY )
  {
   if( (strlen( &eptr->buf[eptr->didx] ) * eptr->cwidth) > (unsigned int) eptr->wwidth - 5 )
    eptr->didx++;
   else
    if( eptr->didx > 0 )
     eptr->didx--;

   XIsetcolour( eptr->window, eptr->bcolor );   /* set for blank/cursor erase*/

   if( flags & LF_BLANK || eptr->didx > 0 )          /* blank line first? */
     XIdrawr( eptr->window, 4, 5, eptr->cheight + 2, eptr->wwidth,
              XI_OPT_FILL ); 
   else                                        /* erase cursor */
    {
     if( eptr->idx > 0 )
      XIdrawr( eptr->window, 5 + (eptr->cwidth * ((eptr->idx-1) - eptr->didx)),
               5, eptr->cheight + 2, eptr->cwidth, XI_OPT_FILL );
    }

   XIsetcolour( eptr->window, eptr->fcolor ); 
   XIdrawtext( eptr->window, 5, 15, &eptr->buf[eptr->didx] );/*show text string */
  }                             /* end if display flag is set */

cursorx = 5 + (eptr->cwidth * (eptr->idx - eptr->didx));

if( flags & LF_NOCURSOR )            /* turn cursor off */
 {
  XIsetcolour( eptr->window, eptr->bcolor );
  XIdrawr( eptr->window, cursorx, 5,
              eptr->cheight + 2, eptr->cwidth, XI_OPT_FILL | XI_OPT_FLUSH );
 }
else        /* show the cursor */
 {
  XIsetcolour( eptr->window, eptr->fcolor );
  XIdrawr( eptr->window, cursorx, 5,
           eptr->cheight + 2, eptr->cwidth, XI_OPT_FILL | XI_OPT_FLUSH );
 }

}                      /* XIemgr */

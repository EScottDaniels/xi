/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
-----------------------------------------------------------------------------------------
	Mnemonic:	XImovew
	Abstract:	allow a window to be moved
	Date:		4 July 2008 
	Author: 	E. Scott Daniels
-----------------------------------------------------------------------------------------
*/

#include "xiinclud.h"

int XImovew( int wnum, int x, int y )
{
	struct window_blk *wptr;    /* pointer at window information block */
	struct display_blk *dptr;    		/* pointer at the display block */

	if( (wptr = XIvalidw( wnum )) == NULL )
		return XI_ERR_BADWNUM;				/* bzzz wrong */

	if( (dptr = dlist[wptr->dnum]) == NULL )
		return XI_ERR_BADWNUM;

	//fprintf( stderr, "movewindow to %d %d\n", x, y );
	XMoveWindow( dptr->disp, wptr->window, x, y );
	XFlush( dptr->disp ); 

	return XI_OK;
}



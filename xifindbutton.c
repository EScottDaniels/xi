/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XIfindbutton
* Abstract: This routine will locate an active button in the list that has
*           the point described by the x and y coordinates passed in. If
*           more than one button in the list contains the point the first
*           active button (button with the FD_VISIBLE flag set) will be
*           acted on. 
* Returns:  Button ID 
*           XI_ERR_NOBUTTON is returned if no button was pressed.
* Parms:    wnum - window number (index into xiwins.)
*           x,y  - Coordinates of the select point in the window.
*           option- Background, initially hide/display button, flush (draw
*                   before leaving this routine)
* Date:     13 September 1991
* Author:   E. Scott Daniels
*
* Mods:		28 Dec 2018 - Allow for translated origin. (and modernise code 
*				formatting)
****************************************************************************
*/
#include "xiinclud.h"

extern int XIfindbutton( int wnum, int x, int y ) {
	struct window_blk *wptr;    /* pointer at window information block */
	struct button_blk *bptr;    /* pointer into the button list */
	int sulx;					// scaled uper corner
	int suly;

	if( (wptr = XIvalidw( wnum )) == NULL ) {
		return( XI_ERR_BADWNUM );    /* let them know they blew it */
	}

	//x -= wptr->orig_x;
	//y -= wptr->orig_y;

	bptr = wptr->blist;   /* point at the button list */

	while( bptr != NULL ) {
		if( bptr->flags & BF_PAINTED ) {										// check only displayed buttons
			sulx = (bptr->x * wptr->scale) + wptr->orig_x;
			suly = (bptr->y * wptr->scale) + wptr->orig_y;
			if( x >= sulx && y >= suly) {
				if( x <= sulx+(bptr->width * wptr->scale) && y <= suly+(bptr->height * wptr->scale) ) {
					break;														// point in the button's rectangle; done
				}
			}
		}

		bptr = bptr->next;
	}

	if( bptr != NULL ) {
		return bptr->bid;				// button the pointer was on
	}
	
	return XI_ERR_NOBUTTON;			// click not on a soft button
}


/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XIsetolay
* Abstract: This routine is responsible modifing the GC associated with the 
*           window number passed in such that the drawing overlay function
*           is set to the requested functions. Valid functions are defined
*           by X windows and are of the type: GCand, GCxor, GCcopy. GCcopy
*           is the default function when the window is created.
* Parms:    wnum - window number (index into xiwins.
*           fun  - Function to pass to the GC.
* Date:     26 August 1991 
* Author:   E. Scott Daniels
*
****************************************************************************
*/
#include "xiinclud.h"

extern int XIsetolay( wnum, fun )
 int wnum;
 int fun;
{
 struct window_blk *wptr;    /* pointer at window information block */
 XGCValues values;           /* information to pass to x */

 if( (wptr = XIvalidw( wnum )) == NULL )
  return( XI_ERR_BADWNUM );    /* let them know they blew it */

 values.function = fun;        /* set up user request */
 XChangeGC( dlist[wptr->dnum]->disp, wptr->gc, GCFunction, &values );

 return( XI_OK );
}                        /* XIsetolay */

/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
************************************************************************
*  Mnemonic: XIconst
*  Abstract: Internal constants for xi modules. users of the lib 
*		need only xi.h
*  Date:     25 July 1991
*  Mod:		16 May 2008 - separated xi.h stuff 
*
*************************************************************************
*/

#define UNUSED(x) (void)(x)			// allows parms passed to be easily marked as unused to eliminate compiler nonsense.

#ifndef NULL                      /* if not already defined */
#define NULL         (void *) 0   /* null pointer */
#endif

#define EOS           (char) 0     /* end of string */
#define TRUE          1  
#define FALSE         0
#define ERROR         (-1) 

#define MAX_WINDOWS   256         /* number of windows allowed open at once */
#define MAX_DISPLAY   4           /* max number of attached displays */
#define MAX_COLOURS    1024        /* size of our colour look up table */
#define MAX_FONTS		128			// number of fonts we will manager


                                  /* flags for the button block */
#define BF_DISPLAY     0x01       /* button can be painted */
#define BF_PAINTED     0x02       /* button has been painted */
#define BF_BACK        0x04       /* background should be drawn for button */
#define BF_PRESSED     0x08       /* button is currently pressed */
#define BF_LOCKED      0x10       /* toggle button is locked in place */

                                  /* flags used for local within routines */
#define LF_DISPLAY     0x01       /* display of something is needed */
#define LF_BLANK       0x02       /* clear something */
#define LF_NOCURSOR    0x04       /* no cursor displayed on operation */

#define DARK_COLOUR_MASK  0xcacaca
#define xLIGHT_COLOUR_MASK 0x686868
#define LIGHT_COLOUR_MASK 0x484848

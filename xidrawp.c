/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
*
* Mnemonic: XIdrawp
* Abstract: Draw poloygon and poloygon support functions.
* Date:     23 August 1991 
* Author:   E. Scott Daniels
*
*	Mods:	22 Jul 2018  - Added support for double buffering and non-C callers.
*
*/
#include "xiinclud.h"

/*
	Draw a polygon given the points array. Options may indicate filled or not.
*/
extern int XIdrawp( int window, XPoint *points, int count, int option ) {
	struct window_blk *wptr;    /* pointer at the window information */
	Drawable target;
	int		free_pts = 0;

	if( (wptr = XIvalidw( window )) == NULL ) {
		return( XI_ERR_BADWNUM );
	}

	if( option & XI_OPT_BB ) {				// select target based on options
		target = wptr->bbuffer;
	} else {
		target = wptr->window;
	}

	if( wptr->orig_x || wptr->orig_y ) {
		free_pts = 1;
		points = XIpt_xlate( wptr, points, count );
	}

	if( option & XI_OPT_FILL )  {
		XFillPolygon( dlist[wptr->dnum]->disp, target, wptr->gc, points, count, Nonconvex, CoordModeOrigin );
	} else {
		XDrawLines( dlist[wptr->dnum]->disp, target, wptr->gc, points, count, CoordModeOrigin );
	}

	if( option & XI_OPT_FLUSH ) {
		XFlush( dlist[wptr->dnum]->disp );       /* flush if requested */
	}

	if( free_pts ) {
		free( points );
	}

	return( XI_OK );
}                              /* XIdrawp */


// --- these functions support non C languages which might not easily be able to allocate an array of points ---
extern int XIdrawp_voidp( int window, void* points, int count, int option ) {
	XPoint* pts;

	pts = (XPoint *) points;
	return XIdrawp( window, pts, count, option );
}

/*
	XIalloc_pts will allocate an array of n XPoint structs which can eventually be passed to X functions.
*/
extern void* XIpt_alloc( int n ) {
	XPoint*	pts;

	pts = (XPoint *) malloc( sizeof( *pts ) * n );
	return (void *) pts;
}

/*
	Given an array of points, allocate a new array and shift them by the origin offset in 
	the window strcut.
*/
extern void* XIpt_xlate( struct window_blk* wptr, XPoint* src_pts, int n ) {
	XPoint*	pts;
	int i;

	pts = (XPoint *) malloc( sizeof( *pts ) * n );

	for( i = 0; i < n; i++ ) {
		pts[i].x = src_pts[i].x + wptr->orig_x;
		pts[i].y = src_pts[i].y + wptr->orig_y;
	}

	return (void *) pts;
}

/*
	Given a pointer to an array of points, add x,y at element n.
	No bounds checking here!
*/
extern void XIpt_add( void* vpts, int n, int x, int y ) {
	XPoint*	pts;

	pts = (XPoint *) vpts;
	if( !pts ) {
		return;
	}

	pts[n].x = x;
	pts[n].y = y;
}

/*
	Release a point, or an array of points.
*/
extern void XIpt_free( void* vpts ) {
	XPoint*	pts;

	pts = (XPoint *) vpts;
	if( !pts ) {
		return;
	}

	free( pts );
}


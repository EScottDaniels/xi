/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
*
* Mnemonic: XIdrawimage
* Abstract:	These routines support conversion of images to pixmaps and 
*		drawing images into windows.  
*		XInewimage -- create and return an XImage from an rgb or rgba
*		map.
*	
*		XIdrawimage -- draws the image into the window putting the x,y
*		coordinate at the upperleft of the window.
*
*		This routine is responsible for drawing an image into the window;
* Date:     02 May 2008
* Author:   E. Scott Daniels
*
****************************************************************************
*/
#include "xiinclud.h"


/* take an image and map it to a pixmap that then can be displayed by x 
   we return an Ximage that can be passed to xidrawiimage(). 
   height, width -- height/width of the source image
   udepth -- colour depth of the source
   pxmap - pointer to rgb/a pixles
   bpp   - bytes per px rgb == 3, rgba == 4
   scale - 1/n
*/
#define RGB2V(c) (((unsigned int)c/8))			/* convert an rgb value to 1/3 of 16bit colour pix value */

/*
	Update an existing image with a new pixmap, or create a new image and add the
	pixmap if the image pointer is nil.
*/
XImage *XIupdateimage( int window, XImage *image, int height, int width,  int udepth, unsigned char *pxmap, int bpp, double scale )
{
	double scaled_h;
	double scaled_w;
	double step_x;
	double step_y;
	double frac_x = 0;
	double frac_y = 0;
 	struct window_blk *wptr;    	/* pointer at our window information  for the window */
	struct display_blk *display;
	Visual *visual; 
	int	depth;			/* display depth */
	int	x;
	int	y;
	unsigned char	*xpxmap;		/* destination for conversion of rgb/a data */
	unsigned char	*pxp;			/* pointer into source pxmap */
	unsigned  long	p = 0;		
	unsigned char 	*nxt;			/* pointer at next row in source */

	depth = udepth;						/* keep compiler happy - use it, but we don't really use it at the moment */

	if( scale <= 0 )
		scale = 1.0;

 	if( (wptr = XIvalidw( window )) == NULL )
  		return( NULL );

	display = dlist[wptr->dnum];
	visual = DefaultVisual( display->disp, display->screen );
	depth = DefaultDepth( display->disp, display->screen ); 


	pxp = pxmap;
	scaled_h = (double) height * scale;
	scaled_w = (double) width * scale;
	step_x = 1.0/scale;
	step_y = 1.0/scale;

	if( image == NULL )
	{
		xpxmap = (unsigned char *) malloc( sizeof( long ) * scaled_h * scaled_w );
		memset( xpxmap, 0x00, sizeof( long ) * scaled_h * scaled_w );
	 
		// ZPixmap is the Xconstant for format type (why not all upper x11?)
 		image = XCreateImage( display->disp, visual, depth, ZPixmap, 0, (char *) xpxmap, scaled_w, scaled_h, 8, 0  );
		if( image == NULL )
		{
			fprintf( stderr, "xidrawimage: create image returned null pointer: %s\n", strerror( errno ) );
			return NULL;
		}
	}

	switch( depth )
	{
		case 16:
			for( y = 0; y < (int) scaled_h; y++ ) {
				nxt = pxp + (width * bpp * (int) step_y);
				frac_y += step_y - (double) ((int) step_y);
				if( frac_y > 1 ) {
					nxt +=  width * bpp;
					frac_y -= 1;
				}
		
				for( x = 0; x < (int) scaled_w; x++ ) {
					p = (RGB2V( *pxp ) <<11) | (RGB2V( *(pxp+1) ) <<6) | (RGB2V( *(pxp+2)) );
					XPutPixel( image, x, y, p  );
		
					pxp += (int) ((double)bpp * step_x);
					frac_x += step_x - (double) ((int) step_x);
					if( frac_x > 1 ) {
						pxp +=  bpp;
						frac_x -= 1;
					}
				}
		
				pxp = nxt;
			}
			break;

		case 24:
			for( y = 0; y < scaled_h; y += 1 ) {
				nxt = pxp + (width * bpp * (int) step_y);			// next row to capture
				frac_y += step_y - (double) ((int) step_y);
				if( frac_y > 1 ) {									// if step isn't even, skip an extra row like leap year
					nxt +=  width * bpp;
					frac_y -= 1;
				}
		
				for( x = 0; x < scaled_w; x += 1 ) {
					p = ((*pxp & 0xff) <<16) | ((*(pxp+1) & 0xff) <<8) | (*(pxp+2) & 0xff);
					XPutPixel( image, x, y, p  );
		
					pxp += bpp * (int) step_x;						// skip in batches of rgb{a} unites
					frac_x += step_x - (double) ((int) step_x);		// add one like leap year if not even
					if( frac_x > 1 ) {
						pxp +=  bpp;
						frac_x -= 1;
					}
				}

				pxp = nxt;
			}
			break;
	}

  	/*XFlush( display->disp );  */
 	return image;
}       					        

/*
XImage *XInewimage( int window, int height, int width,  int udepth, char *pxmap, int bpp, double scale )
{
	return XIupdateimage( window, NULL,  height, width, udepth, (unsigned char *) pxmap, bpp, scale );
}
*/

/*
	Allow a voide pointer.
*/
XImage *XInewimage( int window, int height, int width,  int udepth, void *pxmap, int bpp, double scale ) {
	return XIupdateimage( window, NULL,  height, width, udepth, (unsigned char *) pxmap, bpp, scale );
}


/*
	Draw the image into the window with the upper left corner of the image at xo,yo.
	startx,y is the xy point in the image that is placed at xo,yo.
*/
void XIdrawimage( int window, XImage *image, int xo, int yo, int startx, int starty )
{
 	struct window_blk *wptr;    /* pointer at the window information */
	struct display_blk *display;
	Drawable target;

 	if( (wptr = XIvalidw( window )) == NULL ) {
		fprintf( stderr, ">>> invalid window\n" );
  		return;
	}

	display = dlist[wptr->dnum];

	target = wptr->bbuffer;
	xo += wptr->orig_x;
	yo += wptr->orig_y;

	XPutImage( display->disp, target, wptr->gc, image, startx, starty, xo, yo, wptr->width, wptr->height );
  	XFlush( display->disp );  
}



/* simple wrapper; allows us to handle anything in the future that we might have 
   done with regard to the image
*/
void XIfreeimage( XImage *image )
{
	if( image != NULL )
		XDestroyImage( image );		/* frees the pxmap too */
}

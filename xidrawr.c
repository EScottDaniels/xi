/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
****************************************************************************
* Mnemonic: XIdrawr
* Abstract: This routine is responsible for drawing a rectangle in the 
*           window identified by the window number passed in.
* Parms:    wnum - window number (index into xiwins.
*           ulx  - X cordinate in the window 
*           uly  - Y coordinate in the window
*           height- Height of the rect
*           Width - Width of the rect
*           option- Fill or not fill option     
* Date:     25 July 1991 (PORT FOM VMS)
* Author:   E. Scott Daniels
*
*	Mods:	28 Dec 2018 - Modernise arg definition.
*
****************************************************************************
*/
#include "xiinclud.h"

extern int XIdrawr( int wnum, int ulx, int uly, int height, int width, int option ) {
	struct window_blk *wptr;    /* pointer at window information block */
	Drawable target;

	if( (wptr = XIvalidw( wnum )) == NULL )
		return( XI_ERR_BADWNUM );    /* let them know they blew it */

	if( option & XI_OPT_BB ) {				// select target based on options
		target = wptr->bbuffer;
	} else {
		target = wptr->window;
	}

	ulx += wptr->orig_x;
	uly += wptr->orig_y;

	if( option & XI_OPT_FILL )  /* if filling the rectangle */
		XFillRectangle( dlist[wptr->dnum]->disp, target, wptr->gc, ulx, uly, width, height );
	else
		XDrawRectangle( dlist[wptr->dnum]->disp, target, wptr->gc, ulx, uly, width, height );

	if( option & XI_OPT_FLUSH )           /* if flush option on */
		XFlush( dlist[wptr->dnum]->disp );   /* flush buffer to window manager */

	return( XI_OK );

}                /* XIdrawr */

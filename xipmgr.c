/*
=================================================================================================
	(c) Copyright 1995-2011 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/
/*
***************************************************************************
*
*  Mnemonic: XIpmgr 
*  Abstract: This routine is responsible for managing events that occur
*            on a prompt window. The address of the routine is registered
*            when a prompt block is created and the prompt window is 
*            opened.
*  Parms:    pptr  - Pointer to the prompt block (data)
*            dnum  - Display on which the event occured
*            wnum  - Window on which the event occred 
*            eptr  - Pointer to the event block describing the event
*  Returns:  XI_OK 
*  Date:     23 January 1997
*  Author:   E. Scott Daniels
*
*****************************************************************************
*/
#include "xiinclud.h"   /* get necessary header info */

int XIpmgr( struct prompt_blk *pptr, int dnum, int wnum, XEvent *eptr )
{
 //XButtonEvent *bevent;   /* pointer at specific type of event block */
 //XKeyEvent *kevent;      /* pointer to key event */
 int ((*cbrtn)());       /* pointer to the callback routine */
 //int bid;                /* id of button pressed */
 int status;             /* status to pass to the completion callback */ 

	if( dnum < 0 )
		return 0;

 if( eptr->type == ButtonPress || eptr->type == ButtonRelease )
  {
   //bevent = (XButtonEvent *) eptr;   /* "convert" pointer */
   if( (status = XIbtnmgr( wnum, (XButtonEvent *) eptr )) != XI_ERR_NOBUTTON &&
       eptr->type == ButtonRelease )
    {
     if( pptr->eptr != NULL )              /* was there an edit window? */
      {
       XIclosew( pptr->eptr->window );     /* close it */
       free( pptr->eptr );                 /* free the block too */
      }

     XIclosew( wnum );                     /* close the prompt window */
     if( (cbrtn = pptr->cbrtn) != NULL )   /* is there a callback to drive? */
      (*cbrtn)( pptr->cbdata, status );    /* make the callback */

     if( pptr->text != NULL )
      free( pptr->text );                  /* if we alloced a copy- trash it */
     free( pptr );                         /* and  trash prompt block */
    } 
  }                  /* end if button event */
 else  /* assume expose event */
  {
   XIsetcolour( wnum, pptr->fcolor );      /* set color and draw prompt text */
   XIdrawtext( wnum, 5, 12,  pptr->text );
   XIdispbutton( wnum, 0, XI_OPT_ALL );   /* display all in the window */
  }

 return( XI_OK );
}                   /* xipmgr */
